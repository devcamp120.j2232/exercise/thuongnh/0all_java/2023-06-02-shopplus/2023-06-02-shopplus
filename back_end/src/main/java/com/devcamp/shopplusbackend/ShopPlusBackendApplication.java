package com.devcamp.shopplusbackend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

@SpringBootApplication
@EnableJpaAuditing
public class ShopPlusBackendApplication {

    public static void main(String[] args) {
        SpringApplication.run(ShopPlusBackendApplication.class, args);
    }

}



// //**Lớp ShopPlusBackendApplication là lớp chính của ứng dụng. Nó được sử dụng để khởi động ứng dụng Spring Boot. 
//Trong lớp này, chúng ta có phương thức main được gọi khi ứng dụng được chạy. Phương thức này sử dụng lớp SpringApplication để khởi động ứng dụng Spring Boot.

// Lớp ShopPlusBackendApplication cũng được đánh dấu bằng annotation @SpringBootApplication, cho phép Spring Boot tự động cấu hình ứng dụng.
// Ngoài ra, annotation @EnableJpaAuditing được sử dụng để kích hoạt tính năng auditing trong JPA, giúp tự động quản lý thông tin thời gian tạo và cập nhật của các entity.

// Tổng quan, lớp ShopPlusBackendApplication là điểm khởi đầu của ứng dụng và cung cấp cấu hình ban đầu cho ứng dụng Spring Boot.  **//