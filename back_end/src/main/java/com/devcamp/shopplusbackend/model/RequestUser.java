package com.devcamp.shopplusbackend.model;

import lombok.Getter;
import lombok.Setter;

import java.util.Set;

@Getter
@Setter
public class RequestUser {
    private String firstName;
    private String lastName;
	private String username;
	private String password;
	private boolean activated;
	private String address;
	private String city;
	private String state;
	private String postalCode;
	private String country;
	private Set<String> strRoles;
}
