package com.devcamp.shopplusbackend.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ResponseFile {
    private Long id;
    private String name;
    private String url;
}
