package com.devcamp.shopplusbackend.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ProductSelected {
    private Long productId;

    private Integer quantityOrder;
}
