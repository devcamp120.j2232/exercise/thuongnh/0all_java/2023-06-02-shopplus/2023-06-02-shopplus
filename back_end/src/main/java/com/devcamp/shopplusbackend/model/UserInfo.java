package com.devcamp.shopplusbackend.model;

import lombok.Getter;
import lombok.Setter;

import java.util.Set;

@Getter
@Setter
public class UserInfo {
    private Long id;
    private String firstName;
    private String lastName;
    private String username;
    private String address;
    private String city;
    private String state;
    private String postalCode;
    private String country;
    private Set<String> strRoles;

    public UserInfo(Long id, String firstName, String lastName, String username, String address, String city,
            String state, String postalCode, String country, Set<String> strRoles) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.username = username;
        this.address = address;
        this.city = city;
        this.state = state;
        this.postalCode = postalCode;
        this.country = country;
        this.strRoles = strRoles;
    }
}
