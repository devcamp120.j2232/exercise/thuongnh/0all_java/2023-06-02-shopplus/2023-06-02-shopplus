package com.devcamp.shopplusbackend.model;

public enum OrderStatus {
    Pending, Completed, Refund, Canceled
}
