package com.devcamp.shopplusbackend.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Result {
    private int totalPages;
    private int numberOfElements;
    private long totalElements;
    private Object content;

    public Result(int totalPages, int numberOfElements, long totalElements, Object content) {
        this.totalPages = totalPages;
        this.numberOfElements = numberOfElements;
        this.totalElements = totalElements;
        this.content = content;
    }
}
