package com.devcamp.shopplusbackend.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

@EnableGlobalMethodSecurity(prePostEnabled = true)
// là một lớp cấu hình trong Spring Security. Nó kích hoạt việc cấu hình phân quyền dựa trên phương thức, 
//sử dụng JWT (JSON Web Token) để xác thực. Lớp này cũng cấu hình việc sử dụng JwtRequestFilter để kiểm tra và xác thực JWT trong các yêu cầu. Ngoài ra, 
//nó cũng khai báo một PasswordEncoder sử dụng thuật toán BCrypt để mã hóa mật khẩu.
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private JwtRequestFilter jwtRequestFilter;

    @Override
    public void configure(HttpSecurity http) throws Exception {
        // Thêm JwtRequestFilter trước UsernamePasswordAuthenticationFilter trong chuỗi Filter
        http.addFilterBefore(jwtRequestFilter, UsernamePasswordAuthenticationFilter.class)
            // Vô hiệu hóa CSRF (Cross-Site Request Forgery) protection
            .csrf().disable();
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        // Khởi tạo một PasswordEncoder sử dụng thuật toán BCrypt
        return new BCryptPasswordEncoder();
    }
}
