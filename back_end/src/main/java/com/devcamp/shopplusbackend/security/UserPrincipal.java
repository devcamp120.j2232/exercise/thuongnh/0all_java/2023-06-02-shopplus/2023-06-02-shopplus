package com.devcamp.shopplusbackend.security;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;

@Getter
@Setter

// Lớp UserPrincipal là một implementaion của UserDetails trong Spring Security. Nó đại diện cho thông tin người dùng được xác thực.
public class UserPrincipal implements UserDetails {
    private Long id; // Đại diện cho ID của người dùng.

    private String username; // Đại diện cho tên người dùng.

    @JsonIgnore
    private String password; // Đại diện cho mật khẩu người dùng, được đánh dấu để không được sử dụng khi chuyển đổi thành JSON.

    private Collection authorities; // Đại diện cho các quyền (authorities) của người dùng.

    private boolean deleted; // Đại diện cho trạng thái người dùng đã bị xóa hay chưa.

    private boolean activated; // Đại diện cho trạng thái kích hoạt của người dùng.

    @Override
    public String getUsername() {
        return username; // Trả về tên người dùng.
    }

    @Override
    public boolean isAccountNonExpired() {
        return false; // Kiểm tra xem tài khoản người dùng có hết hạn hay không (luôn trả về false).
    }

    @Override
    public boolean isAccountNonLocked() {
        return !deleted; // Kiểm tra xem tài khoản người dùng có bị khóa hay không (trả về true nếu không bị xóa).
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return false; // Kiểm tra xem thông tin xác thực của người dùng có hết hạn hay không (luôn trả về false).
    }

    @Override
    public boolean isEnabled() {
        return activated; // Kiểm tra xem người dùng có được kích hoạt hay không.
    }
}
