package com.devcamp.shopplusbackend.security;

import org.springframework.data.domain.AuditorAware;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import java.util.Optional;
// Lớp SpringSecurityAuditorAware là một implementaion của AuditorAware trong Spring Data. Nó được sử dụng để cung cấp thông tin về người dùng hiện tại cho các trường createdBy và updatedBy trong các entity.
@Component
public class SpringSecurityAuditorAware implements AuditorAware<Long> {

    @Override
    public Optional<Long> getCurrentAuditor() {
        // Lấy thông tin xác thực từ SecurityContextHolder
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        // Kiểm tra xem người dùng có được xác thực hay không
        if (authentication == null || !authentication.isAuthenticated()) {
            return Optional.empty();
        }
        // Kiểm tra xem người dùng có phải là anonymousUser hay không
        if (authentication.getPrincipal() == "anonymousUser") {
            return Optional.of(0L);
        }
        // Trả về ID của người dùng hiện tại từ UserPrincipal
        return Optional.of(((UserPrincipal) authentication.getPrincipal()).getId());
    }
}
