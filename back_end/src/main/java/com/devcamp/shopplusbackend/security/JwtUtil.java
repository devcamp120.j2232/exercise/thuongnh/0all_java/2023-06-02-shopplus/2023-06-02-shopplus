package com.devcamp.shopplusbackend.security;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.nimbusds.jose.*;
import com.nimbusds.jose.crypto.MACSigner;
import com.nimbusds.jose.crypto.MACVerifier;
import com.nimbusds.jwt.JWTClaimsSet;
import com.nimbusds.jwt.SignedJWT;
import net.minidev.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.text.ParseException;
import java.util.Date;

@Component  // Đánh dấu lớp này là một Bean trong Spring
//  Lớp JwtUtil trong Java được sử dụng để tạo, xác minh và lấy thông tin từ JWT (Json Web Tokens), một cách thức xác thực người dùng phổ biến.
public class JwtUtil {
    private static final Logger logger = LoggerFactory.getLogger(JwtUtil.class);  // Logger để ghi log cho lớp JwtUtil
    private static final String USER = "user";  // Chuỗi constant USER được sử dụng như một key trong payload của JWT
    private static final String SECRET = "daycaidaynaychinhlachukycuabandungdelorangoaidaynhenguyhiemchetnguoidayhihihi";  // Secret key dùng để ký và xác thực JWT

    // Hàm này dùng để tạo token JWT từ thông tin người dùng 
    public String generateToken(UserPrincipal user) {
        String token = null;
        try {
            JWTClaimsSet.Builder builder = new JWTClaimsSet.Builder();  // Khởi tạo Builder cho JWTClaimsSet
            builder.claim(USER, user);  // Đặt claim USER với giá trị là user
            builder.expirationTime(generateExpirationDate());  // Đặt thời gian hết hạn cho token
            JWTClaimsSet claimsSet = builder.build();  // Xây dựng JWTClaimsSet từ builder
            SignedJWT signedJWT = new SignedJWT(new JWSHeader(JWSAlgorithm.HS256), claimsSet);  // Tạo SignedJWT từ JWSHeader và JWTClaimsSet
            JWSSigner signer = new MACSigner(SECRET.getBytes());  // Khởi tạo signer với secret key
            signedJWT.sign(signer);  // Ký JWT bằng signer
            token = signedJWT.serialize();  // Chuyển đổi JWT thành chuỗi
        } catch (Exception e) {
            logger.error(e.getMessage());  // Ghi lỗi vào log nếu có
        }
        return token;  // Trả về token
    }

    // Hàm này trả về thời điểm hết hạn của token, đặt là 10 ngày sau thời điểm hiện tại
    public Date generateExpirationDate() {
        return new Date(System.currentTimeMillis() + 864000000);
    }

    // Hàm này dùng để lấy ra thông tin từ token
    private JWTClaimsSet getClaimsFromToken(String token) {
        JWTClaimsSet claims = null;
        try {
            SignedJWT signedJWT = SignedJWT.parse(token);  // Parse token thành đối tượng SignedJWT
            JWSVerifier verifier = new MACVerifier(SECRET.getBytes());  // Khởi tạo verifier với secret key
            if (signedJWT.verify(verifier)) {  // Kiểm tra xem JWT có được ký bởi secret key này hay không
                claims = signedJWT.getJWTClaimsSet();  // Nếu có thì lấy ra thông tin từ token
            }
        } catch (ParseException | JOSEException e) {
            logger.error(e.getMessage());  // Ghi lỗi vào log nếu có
        }
        return claims;  // Trả về thông tin từ token
    }

    // Hàm này dùng để lấy ra thông tin người dùng từ token
    public UserPrincipal getUserFromToken(String token) {
        UserPrincipal user = null;
        try {
            JWTClaimsSet claims = getClaimsFromToken(token);  // Lấy ra thông tin từ token
            if (claims != null && isTokenExpired(claims)) {  // Kiểm tra xem token có thông tin và có hết hạn hay không
                JSONObject jsonObject = (JSONObject) claims.getClaim(USER);  // Lấy ra thông tin người dùng từ thông tin token
                user = new ObjectMapper().readValue(jsonObject.toJSONString(), UserPrincipal.class);  // Chuyển đổi thông tin người dùng từ chuỗi JSON sang đối tượng UserPrincipal
            }
        } catch (Exception e) {
            logger.error(e.getMessage());  // Ghi lỗi vào log nếu có
        }
        return user;  // Trả về thông tin người dùng
    }

    // Hàm này dùng để lấy ra thời gian hết hạn của token từ thông tin token
    private Date getExpirationDateFromToken(JWTClaimsSet claims) {
        return claims != null ? claims.getExpirationTime() : new Date();  // Nếu thông tin token không null thì lấy ra thời gian hết hạn, nếu không thì trả về thời gian hiện tại
    }

    // Hàm này dùng để kiểm tra xem token có hết hạn hay không
    private boolean isTokenExpired(JWTClaimsSet claims) {
        return getExpirationDateFromToken(claims).after(new Date());  // Kiểm tra xem thời gian hết hạn có sau thời gian hiện tại hay không
    }
}
