package com.devcamp.shopplusbackend.security;

import com.devcamp.shopplusbackend.entity.Token;
import com.devcamp.shopplusbackend.service.TokenService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;


// Đây là một Filter trong Spring Security dùng để xử lý yêu cầu và xác thực JWT (JSON Web Token). Dưới đây là ý nghĩa của từng dòng code:
@Component
public class JwtRequestFilter extends OncePerRequestFilter {
    @Autowired
    private JwtUtil jwtUtil;

    @Autowired
    private TokenService verificationTokenService;

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        // Lấy giá trị của tiêu đề "Authorization" từ yêu cầu
        final String authorizationHeader = request.getHeader("Authorization");

        UserPrincipal user = null;
        Token token = null;
        if (StringUtils.hasText(authorizationHeader) && authorizationHeader.startsWith("Bearer ")) {
            // Tách chuỗi JWT từ tiêu đề "Authorization"
            String jwt = authorizationHeader.substring(7);
            // Lấy thông tin người dùng từ JWT
            user = jwtUtil.getUserFromToken(jwt);
            // Lấy thông tin token từ cơ sở dữ liệu dựa trên JWT
            token = verificationTokenService.findByToken(jwt);
        }

        // Kiểm tra xác thực và trạng thái của người dùng và token
        if (null != user && user.isEnabled() && user.isAccountNonLocked() && null != token && token.getTokenExpDate().after(new Date()) && !token.isDeleted()) {
            // Chuyển đổi danh sách quyền thành đối tượng GrantedAuthority
            Set<GrantedAuthority> authorities = new HashSet<>();
            user.getAuthorities().forEach(p -> authorities.add(new SimpleGrantedAuthority((String) p)));
            // Xác thực người dùng với thông tin xác thực và danh sách quyền
            UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(user, null, authorities);
            // Cung cấp thông tin xác thực từ yêu cầu
            authentication.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
            // Đặt thông tin xác thực vào SecurityContextHolder
            SecurityContextHolder.getContext().setAuthentication(authentication);
        }

        // Chuyển tiếp yêu cầu đến Filter tiếp theo trong chuỗi Filter
        filterChain.doFilter(request, response);
    }
}
