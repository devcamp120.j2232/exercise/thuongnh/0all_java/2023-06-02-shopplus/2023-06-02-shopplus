package com.devcamp.shopplusbackend.repository;

import com.devcamp.shopplusbackend.entity.GraphicsType;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface GraphicsTypeRepository extends JpaRepository<GraphicsType, Long> {
    @Query(value = "FROM GraphicsType gt WHERE gt.name LIKE %:keyword%")
    Page<GraphicsType> findGraphicsTypes(String keyword, Pageable pageable);

    boolean existsByName(String name);
}
