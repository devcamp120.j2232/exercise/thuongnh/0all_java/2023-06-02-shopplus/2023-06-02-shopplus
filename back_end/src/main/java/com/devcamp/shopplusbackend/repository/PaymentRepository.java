package com.devcamp.shopplusbackend.repository;

import com.devcamp.shopplusbackend.entity.Payment;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface PaymentRepository extends JpaRepository<Payment, Long> {
    @Query(value = "FROM Payment p WHERE p.checkNumber LIKE %:keyword% ORDER BY p.checkNumber")
    Page<Payment> findPayments(String keyword, Pageable pageable);
    
    boolean existsByCheckNumber(String checkNumber);
}
