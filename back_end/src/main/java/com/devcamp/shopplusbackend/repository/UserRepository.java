package com.devcamp.shopplusbackend.repository;

import com.devcamp.shopplusbackend.entity.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.sql.Date;
import java.util.List;

public interface UserRepository extends JpaRepository<User, Long> {
    //Lấy danh sách tài khoản người dùng
    @Query(value = "SELECT u FROM User u WHERE (CONCAT(u.firstName, ' ', u.lastName) LIKE %:keyword% OR CONCAT(u.lastName, ' ', u.firstName) LIKE %:keyword% OR u.username LIKE %:keyword%)")
    Page<User> findUsers(String keyword, Pageable pageable);

    //Lấy danh sách người dùng theo số tiền mua hàng
    @Query(value = "SELECT u FROM User u JOIN Order o ON u.id = o.user.id JOIN OrderDetail od ON o.id = od.order.id WHERE (CONCAT(u.firstName, ' ', u.lastName) LIKE %:keyword% OR CONCAT(u.lastName, ' ', u.firstName) LIKE %:keyword% OR u.username LIKE %:keyword%) AND (o.status = 'Completed') GROUP BY u.id HAVING SUM(od.priceEach * od.quantityOrder) > :totalSales")
    Page<User> findUsersByCustomerType(String keyword, BigDecimal totalSales, Pageable pageable);

    //Lấy thông tin người dùng theo username
    User findByUsername(String username);

    //Lấy số lượng tài khoản đã đăng ký theo thời gian
    @Query(value = "SELECT COUNT(u) FROM User u WHERE (u.createdAt BETWEEN :startDate AND :endDate)")
    Long getTotalUsers(Date startDate, Date endDate);

    //Enable/Disable tài khoản
    @Transactional
    @Modifying
    @Query(value = "UPDATE User u SET u.deleted = :deleted WHERE u.id = :id")
    void disableOrEnableUser(Long id, boolean deleted);

    //Lấy danh sách người dùng bởi select tìm kiếm
    @Query(value = "FROM User u WHERE CONCAT(u.firstName, ' ', u.lastName) LIKE %:keyword% OR CONCAT(u.lastName, ' ', u.firstName) LIKE %:keyword% OR u.username LIKE %:keyword%")
    List<User> selectSearchCustomers(String keyword);

    boolean existsByUsername(String username);
}
