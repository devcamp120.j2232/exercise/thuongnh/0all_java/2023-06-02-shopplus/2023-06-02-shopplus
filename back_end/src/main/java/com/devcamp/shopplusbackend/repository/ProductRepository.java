package com.devcamp.shopplusbackend.repository;

import com.devcamp.shopplusbackend.entity.Product;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.sql.Date;
import java.util.List;

@Repository
public interface ProductRepository extends JpaRepository<Product, Long> {
    //Lấy danh sách sản phẩm
    @Query(value = "FROM Product p WHERE (p.productCode LIKE %:keyword% OR CONCAT(p.productBrand.name, ' ', p.productName)  LIKE %:keyword% OR CONCAT(p.productBrand.name, ' ', p.productLine.name, ' ', p.productName) LIKE %:keyword%) AND :productBrandId IN(p.productBrand.id, -1) AND :processorBrandId IN(p.processor.processorBrand.id, -1) AND p.deleted = :deleted")
    Page<Product> findProducts(String keyword, Long productBrandId, Long processorBrandId, boolean deleted, Pageable pageable);

    //Lấy danh sách sản phẩm
    @Query(value = "FROM Product p WHERE (p.productCode LIKE %:keyword% OR CONCAT(p.productBrand.name, ' ', p.productName)  LIKE %:keyword% OR CONCAT(p.productBrand.name, ' ', p.productLine.name, ' ', p.productName) LIKE %:keyword%) AND :productBrandId IN(p.productBrand.id, -1) AND :productLineId IN(p.productLine.id, -1) AND (p.screenSize BETWEEN :minScreenSize AND :maxScreenSize) AND (p.buyPrice BETWEEN :minPrice AND :maxPrice) AND :memory IN(p.systemMemory, -1) AND :storageSize IN(p.totalStorageCapacity, -1) AND p.quantityInStock > 0 AND p.deleted = false")
    Page<Product> searchProducts(String keyword, Long productBrandId, Long productLineId, BigDecimal minScreenSize, BigDecimal maxScreenSize, BigDecimal minPrice, BigDecimal maxPrice, Integer memory, Integer storageSize, Pageable pageable);

    //Lấy top sản phẩm theo số lượng đơn hàng bởi người dùng
    @Query(value = "SELECT p FROM Product p JOIN OrderDetail od ON p.id = od.product.id JOIN Order o ON od.order.id = o.id WHERE (p.deleted = false AND o.orderDate BETWEEN :startDate AND :endDate) GROUP BY od.product.id ORDER BY COUNT(p) DESC")
    Page<Product> bestSellersProducts(Date startDate, Date endDate, Pageable pageable);

    //Lấy top sản phẩm theo số lượng đơn hàng bởi quản trị viên
    @Query(value = "SELECT p FROM Product p JOIN OrderDetail od ON p.id = od.product.id JOIN Order o ON od.order.id = o.id WHERE (o.orderDate BETWEEN :startDate AND :endDate) AND (o.status = 'Completed') GROUP BY od.product.id ORDER BY COUNT(p) DESC")
    Page<Product> topSellingProductsByTotalOrders(Date startDate, Date endDate, Pageable pageable);

    @Query(value = "SELECT p FROM Product p JOIN OrderDetail od ON p.id = od.product.id JOIN Order o ON od.order.id = o.id WHERE (o.orderDate BETWEEN :startDate AND :endDate) AND (o.status = 'Completed') GROUP BY od.product.id ORDER BY SUM(od.priceEach * od.quantityOrder) DESC")
    Page<Product> topSellingProductsByTotalSales(Date startDate, Date endDate, Pageable pageable);

    @Query(value = "SELECT COUNT(p) FROM Product p JOIN OrderDetail od ON p.id = od.product.id JOIN Order o ON od.order.id = o.id WHERE (o.orderDate BETWEEN :startDate AND :endDate) AND od.product.id = :productId AND (o.status = 'Completed') GROUP BY od.product.id")
    Long getTotalOrders(Date startDate, Date endDate, Long productId);

    @Query(value = "SELECT SUM(od.priceEach * od.quantityOrder) FROM Product p JOIN OrderDetail od ON p.id = od.product.id JOIN Order o ON od.order.id = o.id WHERE (o.orderDate BETWEEN :startDate AND :endDate) AND od.product.id = :productId AND (o.status = 'Completed') GROUP BY od.product.id")
    BigDecimal getTotalSales(Date startDate, Date endDate, Long productId);

    @Query(value = "FROM Product p WHERE p.productCode LIKE %:keyword% OR CONCAT(p.productBrand.name, ' ', p.productName)  LIKE %:keyword% OR CONCAT(p.productBrand.name, ' ', p.productLine.name, ' ', p.productName) LIKE %:keyword% AND p.quantityInStock > 0 AND p.deleted = false")
    List<Product> selectSearchProducts(String keyword);

    //Kiểm tra mã sản phẩm đã tồn tại hay chưa
    boolean existsByProductCode(String productCode);

    //Thay đổi số lượng sản phẩm trong kho
    @Transactional
    @Modifying
    @Query(value = "UPDATE Product p SET p.quantityInStock = p.quantityInStock + :quantity WHERE p.id = :id")
    void changeQuantityInStock(Long id, Integer quantity);

    //Enable/Disable sản phẩm
    @Transactional
    @Modifying
    @Query(value = "UPDATE Product p SET p.deleted = :deleted WHERE p.id = :id")
    void disableOrEnableProduct(Long id, boolean deleted);
}
