package com.devcamp.shopplusbackend.repository;

import com.devcamp.shopplusbackend.entity.ProductBrand;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface ProductBrandRepository extends JpaRepository<ProductBrand, Long> {
    @Query(value = "FROM ProductBrand pb WHERE pb.name LIKE %:keyword%")
    Page<ProductBrand> findProductBrands(String keyword, Pageable pageable);

    boolean existsByName(String name);
}
