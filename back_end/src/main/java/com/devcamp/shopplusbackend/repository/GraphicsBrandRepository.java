package com.devcamp.shopplusbackend.repository;

import com.devcamp.shopplusbackend.entity.GraphicsBrand;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface GraphicsBrandRepository extends JpaRepository<GraphicsBrand, Long> {
    @Query(value = "FROM GraphicsBrand gb WHERE gb.name LIKE %:keyword%")
    Page<GraphicsBrand> findGraphicsBrands(String keyword, Pageable pageable);

    boolean existsByName(String name);
}
