package com.devcamp.shopplusbackend;

import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

public class ServletInitializer extends SpringBootServletInitializer {

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(ShopPlusBackendApplication.class);
    }

}

// Lớp ServletInitializer là một lớp mở rộng của SpringBootServletInitializer trong Spring Boot. 
//Nó được sử dụng để cấu hình và khởi tạo ứng dụng web Spring Boot khi nó được triển khai trong một máy chủ web (ví dụ: Tomcat, Jetty).