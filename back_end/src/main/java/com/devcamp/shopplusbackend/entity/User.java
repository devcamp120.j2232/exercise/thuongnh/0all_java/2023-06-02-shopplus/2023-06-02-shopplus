package com.devcamp.shopplusbackend.entity;

import com.devcamp.shopplusbackend.model.OrderStatus;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "users")
@Getter
@Setter
public class User extends BaseEntity {
	@NotBlank
	@Size(max = 20)
	@Column(length = 20, nullable = false)
	private String firstName;

	@NotBlank
	@Size(max = 20)
	@Column(length = 20, nullable = false)
	private String lastName;

	@NotBlank
	@Size(max = 50)
	@Column(length = 50, unique = true, updatable = false, nullable = false)
	private String username;

	@NotBlank
	@Size(max = 255)
	@Column(length = 255, nullable = false)
	private String password;

	@Column(columnDefinition = "boolean default false")
	private boolean activated = false;

	@Column(length = 255)
	private String address;

	@Column(length = 50)
	private String city;

	@Column(length = 50)
	private String state;

	@Column(length = 50)
	private String postalCode;

	@Column(length = 50)
	private String country;

	@ManyToMany
	@JoinTable(name = "user_roles", joinColumns = @JoinColumn(name = "user_id"), inverseJoinColumns = @JoinColumn(name = "role_id"))
	private Set<Role> roles = new HashSet<>();

	@OneToMany(mappedBy = "user")
	@JsonIgnore
	private Set<Order> orders;

	@OneToMany(mappedBy = "user")
	@JsonIgnore
	private Set<Payment> payments;

	@OneToOne(cascade = CascadeType.REMOVE, mappedBy = "user")
	private UserPhoto userPhoto;

	@Transient
	private int totalOrders;

	@Transient
	private BigDecimal totalSales;

	public int getTotalOrders() {
		if (orders != null) {
			return orders.size();
		}
		return 0;
	}

	public BigDecimal getTotalSales() {
		if (orders == null) {
			return BigDecimal.valueOf(0);
		}
		final BigDecimal[] totalSales = { BigDecimal.valueOf(0) };
		orders.forEach(order -> {
			if (order.getStatus() == OrderStatus.Completed) {
				final BigDecimal[] sales = { new BigDecimal(0) };
				order.getOrderDetails().forEach(orderDetail -> {
					sales[0] = sales[0].add(
							orderDetail.getPriceEach().multiply(BigDecimal.valueOf(orderDetail.getQuantityOrder())));
				});
				totalSales[0] = totalSales[0].add(sales[0]);
			}
		});
		return totalSales[0];
	}
}
