package com.devcamp.shopplusbackend.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "processor_collections")
@Getter
@Setter
public class ProcessorCollection {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(length = 50, unique = true, nullable = false)
    private String name;

    @Column(length = 2500)
    private String description;

    @ManyToOne
    @JoinColumn(name = "processor_brand_id", nullable = false)
    private ProcessorBrand processorBrand;

    @OneToMany(mappedBy = "processorCollection")
    @JsonIgnore
    private Set<Processor> processors;

    @Transient
    private String fullName;

    public String getFullName() {
        if (processorBrand == null) {
            return name;
        }
        return processorBrand.getName() + " " + name;
    }
}