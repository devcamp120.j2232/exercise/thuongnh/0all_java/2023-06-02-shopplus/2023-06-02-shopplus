package com.devcamp.shopplusbackend.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "graphics_types")
@Getter
@Setter
public class GraphicsType {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "graphics_brand_id")
    private GraphicsBrand graphicsBrand;

    @Column(length = 50, nullable = false, unique = true)
    private String name;

    @Column(length = 2500)
    private String description;

    @OneToMany(mappedBy = "graphicsType")
    @JsonIgnore
    private Set<Graphics> graphics;

    @Transient
    private String fullName;

    public String getFullName() {
        if (graphicsBrand == null) {
            return name;
        }
        return graphicsBrand.getName() + " " + name;
    }
}
