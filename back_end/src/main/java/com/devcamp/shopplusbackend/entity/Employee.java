package com.devcamp.shopplusbackend.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.Size;

@Entity
@Table(name = "employees")
@Getter
@Setter
public class Employee {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(length = 50, nullable = false)
    private String firstName;

    @Column(length = 50, nullable = false)
    private String lastName;

    @Column(length = 50, nullable = false)
    private String extension;

    @Column(length = 50, unique = true, nullable = false)
    private String email;

    @ManyToOne
    @JoinColumn(name = "office_code", nullable = false)
    private Office office;

    private Long reportTo;

    @Size(max = 50)
    private String jobTitle;
}
