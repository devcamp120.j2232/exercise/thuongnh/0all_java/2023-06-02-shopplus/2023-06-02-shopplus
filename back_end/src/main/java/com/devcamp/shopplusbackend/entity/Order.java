package com.devcamp.shopplusbackend.entity;

import com.devcamp.shopplusbackend.model.OrderStatus;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.CreatedDate;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;
import java.util.Set;

@Entity
@Table(name = "orders")
@Getter
@Setter
public class Order extends BaseEntity {
	@Column(length = 50, unique = true, updatable = false, nullable = false)
	private String orderCode;

	@Temporal(TemporalType.TIMESTAMP)
	@CreatedDate
	@JsonFormat(pattern = "yyyy-MM-dd")
	@Column(updatable = false, nullable = false)
	private Date orderDate;

	@Temporal(TemporalType.TIMESTAMP)
	@JsonFormat(pattern = "yyyy-MM-dd")
	@Column(nullable = false)
	private Date requiredDate;

	@Temporal(TemporalType.TIMESTAMP)
	@JsonFormat(pattern = "yyyy-MM-dd")
	private Date shippedDate;

	@Enumerated(EnumType.STRING)
	@Column(nullable = false)
	private OrderStatus status;

	@Column(length = 255)
	private String comments;

	@ManyToOne
	@JoinColumn(name = "user_id", nullable = false)
	private User user;

	@OneToMany(mappedBy = "order")
	@JsonIgnore
	private Set<OrderDetail> orderDetails;

	@Column(length = 20, nullable = false)
	private String firstName;

	@Column(length = 20, nullable = false)
	private String lastName;

	@Column(length = 20, nullable = false)
	private String phoneNumber;

	@Column(length = 255, nullable = false)
	private String address;

	@Column(length = 50)
	private String city;

	@Column(length = 50)
	private String state;

	@Column(length = 50)
	private String postalCode;

	@Column(length = 50)
	private String country;

	@Transient
	private BigDecimal totalOrderAmount;

	public BigDecimal getTotalOrderAmount() {
		if (orderDetails != null && orderDetails.size() > 0) {
			final BigDecimal[] totalOrderAmount = { BigDecimal.valueOf(0) };
			orderDetails.forEach(orderDetail -> {
				totalOrderAmount[0] = totalOrderAmount[0]
						.add(orderDetail.getPriceEach().multiply(BigDecimal.valueOf(orderDetail.getQuantityOrder())));
			});
			return totalOrderAmount[0];
		}
		return BigDecimal.valueOf(0);
	}
}
