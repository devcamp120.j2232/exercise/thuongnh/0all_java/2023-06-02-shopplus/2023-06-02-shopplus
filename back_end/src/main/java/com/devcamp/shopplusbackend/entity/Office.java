package com.devcamp.shopplusbackend.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "offices")
@Getter
@Setter
public class Office {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(length = 50, nullable = false)
    private String city;

    @Column(length = 50, unique = true, nullable = false)
    private String phone;

    @Column(length = 255, nullable = false)
    private String addressLine;

    @Column(length = 50)
    private String territory;

    @Column(length = 50)
    private String state;

    @Column(length = 50, nullable = false)
    private String country;

    @OneToMany(mappedBy = "office")
    @JsonIgnore
    private Set<Employee> employees;
}
