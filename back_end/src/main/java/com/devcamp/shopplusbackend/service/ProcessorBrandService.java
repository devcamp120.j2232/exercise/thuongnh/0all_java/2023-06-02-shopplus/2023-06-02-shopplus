package com.devcamp.shopplusbackend.service;

import com.devcamp.shopplusbackend.entity.ProcessorBrand;
import com.devcamp.shopplusbackend.entity.ProcessorCollection;
import com.devcamp.shopplusbackend.repository.ProcessorBrandRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.Set;

@Service
public class ProcessorBrandService {
    @Autowired
    private ProcessorBrandRepository processorBrandRepository;

    public List<ProcessorBrand> getAllProcessorBrands() {
        return processorBrandRepository.findAll();
    }

    public Page<ProcessorBrand> getProcessorBrands(String keyword, int page, int size) {
        Pageable pageable = PageRequest.of(page, size);
        return processorBrandRepository.findProcessorBrands(keyword, pageable);
    }

    public ProcessorBrand getProcessorBrandById(Long id) {
        Optional<ProcessorBrand> processorBrandOptional = processorBrandRepository.findById(id);
        return processorBrandOptional.orElse(null);
    }

    public Set<ProcessorCollection> getProcessorCollections(Long id) {
        Optional<ProcessorBrand> processorBrandOptional = processorBrandRepository.findById(id);
        return processorBrandOptional.map(ProcessorBrand::getProcessorCollections).orElse(null);
    }

    public ProcessorBrand createProcessorBrand(ProcessorBrand pProcessorBrand) {
        pProcessorBrand.setId(null);
        return processorBrandRepository.save(pProcessorBrand);
    }

    public ProcessorBrand updateProcessorBrand(Long id, ProcessorBrand pProcessorBrand) {
        Optional<ProcessorBrand> processorBrandOptional = processorBrandRepository.findById(id);
        if (processorBrandOptional.isPresent()) {
            pProcessorBrand.setId(id);
            return processorBrandRepository.save(pProcessorBrand);
        }
        return null;
    }

    public boolean deleteProcessorBrand(Long id) {
        Optional<ProcessorBrand> processorBrandOptional = processorBrandRepository.findById(id);
        if (processorBrandOptional.isPresent()) {
            processorBrandRepository.deleteById(id);
            return true;
        }
        return false;
    }

    public boolean existsByName(String name) {
        return processorBrandRepository.existsByName(name);
    }
}