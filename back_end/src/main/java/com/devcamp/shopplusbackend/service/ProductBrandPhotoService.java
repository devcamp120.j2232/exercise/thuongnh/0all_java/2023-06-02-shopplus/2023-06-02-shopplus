package com.devcamp.shopplusbackend.service;

import com.devcamp.shopplusbackend.entity.ProductBrand;
import com.devcamp.shopplusbackend.entity.ProductBrandPhoto;
import com.devcamp.shopplusbackend.entity.ProductPhoto;
import com.devcamp.shopplusbackend.model.ResponseFile;
import com.devcamp.shopplusbackend.repository.ProductBrandPhotoRepository;
import com.devcamp.shopplusbackend.repository.ProductBrandRepository;
import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.io.IOException;
import java.util.Objects;
import java.util.Optional;
import java.util.UUID;

@Service
public class ProductBrandPhotoService {
    @Autowired
    private ProductBrandRepository productBrandRepository;

    @Autowired
    private ProductBrandPhotoRepository productBrandPhotoRepository;

    @Autowired
    private StorageService storageService;

    public ResponseFile storeProductBrandPhoto(Long productBrandId, MultipartFile file) throws IOException {
        Optional<ProductBrand> optionalProductBrand = productBrandRepository.findById(productBrandId);
        if (optionalProductBrand.isPresent()) {
            ProductBrand productBrand = optionalProductBrand.get();
            if (productBrand.getProductBrandPhoto() != null) {
                ProductBrandPhoto productBrandPhoto = productBrand.getProductBrandPhoto();
                productBrandPhotoRepository.delete(productBrandPhoto);
            }
            ProductBrandPhoto productBrandPhoto = new ProductBrandPhoto();
            productBrandPhoto.setProductBrand(productBrand);
            String fileType = FilenameUtils.getExtension(file.getOriginalFilename());
            productBrandPhoto.setName(UUID.randomUUID() + "." + fileType);
            ProductBrandPhoto photo = productBrandPhotoRepository.save(productBrandPhoto);
            storageService.store("/product-brand-photos/", photo.getName(), file);
            ResponseFile responseFile = new ResponseFile();
            responseFile.setId(photo.getId());
            responseFile.setName(photo.getName());
            responseFile.setUrl(ServletUriComponentsBuilder.fromCurrentContextPath().path("/product-brand-photos/").path(photo.getName()).toUriString());
            return responseFile;
        }
        return null;
    }

    public Long deleteProductBrandPhoto(Long id) throws IOException {
        Optional<ProductBrandPhoto> optionalProductBrandPhoto = productBrandPhotoRepository.findById(id);
        if (optionalProductBrandPhoto.isPresent()) {
            storageService.delete("/product-brand-photos/", optionalProductBrandPhoto.get().getName());
            productBrandPhotoRepository.deleteById(id);
            return id;
        }
        return null;
    }
}
