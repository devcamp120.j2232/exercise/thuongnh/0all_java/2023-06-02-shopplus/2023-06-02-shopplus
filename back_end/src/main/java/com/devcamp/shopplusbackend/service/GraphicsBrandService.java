package com.devcamp.shopplusbackend.service;

import com.devcamp.shopplusbackend.entity.*;
import com.devcamp.shopplusbackend.repository.GraphicsBrandRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.Set;

@Service
public class GraphicsBrandService {
    @Autowired
    private GraphicsBrandRepository graphicsBrandRepository;

    public List<GraphicsBrand> getAllGraphicsBrands() {
        return graphicsBrandRepository.findAll();
    }

    public Page<GraphicsBrand> getGraphicsBrands(String keyword, int page, int size) {
        Pageable pageable = PageRequest.of(page, size);
        return graphicsBrandRepository.findGraphicsBrands(keyword, pageable);
    }

    public GraphicsBrand getGraphicsBrandById(Long id) {
        Optional<GraphicsBrand> graphicsBrandOptional = graphicsBrandRepository.findById(id);
        return graphicsBrandOptional.orElse(null);
    }

    public Set<GraphicsType> getGraphicsTypes(Long id) {
        Optional<GraphicsBrand> graphicsBrandOptional = graphicsBrandRepository.findById(id);
        return graphicsBrandOptional.map(GraphicsBrand::getGraphicsTypes).orElse(null);
    }

    public GraphicsBrand createGraphicsBrand(GraphicsBrand pGraphicsBrand) {
        pGraphicsBrand.setId(null);
        return graphicsBrandRepository.save(pGraphicsBrand);
    }

    public GraphicsBrand updateGraphicsBrand(Long id, GraphicsBrand pGraphicsBrand) {
        Optional<GraphicsBrand> graphicsBrandOptional = graphicsBrandRepository.findById(id);
        if (graphicsBrandOptional.isPresent()) {
            pGraphicsBrand.setId(id);
            return graphicsBrandRepository.save(pGraphicsBrand);
        }
        return null;
    }

    public boolean deleteGraphicsBrand(Long id) {
        Optional<GraphicsBrand> graphicsBrandOptional = graphicsBrandRepository.findById(id);
        if (graphicsBrandOptional.isPresent()) {
            graphicsBrandRepository.deleteById(id);
            return true;
        }
        return false;
    }

    public boolean existsByName(String name) {
        return graphicsBrandRepository.existsByName(name);
    }
}