package com.devcamp.shopplusbackend.service;

import com.devcamp.shopplusbackend.entity.Payment;
import com.devcamp.shopplusbackend.repository.PaymentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.Optional;

@Service
public class PaymentService {
    @Autowired
    private PaymentRepository paymentRepository;

    public Page<Payment> getPayments(String keyword, int page, int size) {
        Pageable pageable = PageRequest.of(page, size);
        return paymentRepository.findPayments(keyword, pageable);
    }

    public Payment getPaymentById(Long id) {
        Optional<Payment> paymentOptional = paymentRepository.findById(id);
        return paymentOptional.orElse(null);
    }

    public Payment createPayment(Payment pPayment) {
        pPayment.setId(null);
        pPayment.setCheckNumber(checkNumberGenerator());
        pPayment.setPaymentDate(new Date());
        return paymentRepository.save(pPayment);
    }

    public Payment updatePayment(Long id, Payment pPayment) {
        Optional<Payment> paymentOptional = paymentRepository.findById(id);
        if (paymentOptional.isPresent()) {
            pPayment.setId(id);
            return paymentRepository.save(pPayment);
        }
        return null;
    }

    public boolean deletePayment(Long id) {
        Optional<Payment> paymentOptional = paymentRepository.findById(id);
        if (paymentOptional.isPresent()) {
            paymentRepository.deleteById(id);
            return true;
        }
        return false;
    }

    public boolean existsByCheckNumber(String checkNumber) {
        return paymentRepository.existsByCheckNumber(checkNumber);
    }

    private String checkNumberGenerator() {
        String AlphaNumericString = "0123456789abcdefghijklmnopqrstuvwxyz";
        int length = 8;
        StringBuilder sb = new StringBuilder(length);
        for (int i = 0; i < length; i++) {
            int index = (int) (AlphaNumericString.length() * Math.random());
            sb.append(AlphaNumericString.charAt(index));
        }
        return sb.toString();
    }
}
