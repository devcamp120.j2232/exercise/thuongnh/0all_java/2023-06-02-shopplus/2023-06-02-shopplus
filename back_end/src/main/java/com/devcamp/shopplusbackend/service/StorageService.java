package com.devcamp.shopplusbackend.service;

import org.springframework.core.io.Resource;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.net.MalformedURLException;

public interface StorageService {
    void store(String folder, String filename, MultipartFile file);

    Resource loadAsResource(String folder, String filename);

    void delete(String folder, String filename) throws IOException;
}