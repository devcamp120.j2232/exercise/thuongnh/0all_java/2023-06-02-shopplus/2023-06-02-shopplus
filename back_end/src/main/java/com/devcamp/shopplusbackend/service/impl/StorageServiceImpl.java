package com.devcamp.shopplusbackend.service.impl;

import com.devcamp.shopplusbackend.exception.StorageException;
import com.devcamp.shopplusbackend.exception.StorageFileNotFoundException;
import com.devcamp.shopplusbackend.service.StorageService;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.nio.file.*;

@Service
public class StorageServiceImpl implements StorageService {
    private final String uploadPath = "upload-dir";

    @Override
    public void store(String folder, String filename, MultipartFile file) {
        try {
            Path path = Paths.get(uploadPath + folder);
            if (file.isEmpty()) {
                throw new StorageException("Failed to store empty file.");
            }
            if (!Files.exists(path)) {
                Files.createDirectories(path);
            }
            Path destinationFile = path.resolve(Paths.get(filename)).normalize().toAbsolutePath();
            if (!destinationFile.getParent().equals(path.toAbsolutePath())) {
                throw new StorageException("Cannot store file outside current directory.");
            }
            try (InputStream inputStream = file.getInputStream()) {
                Files.copy(inputStream, destinationFile, StandardCopyOption.REPLACE_EXISTING);
            }
        } catch (IOException e) {
            throw new StorageException("Failed to store file.", e);
        }
    }

    @Override
    public Resource loadAsResource(String folder, String filename) {
        try {
            Path path = Paths.get(uploadPath + folder);
            Path file = path.resolve(filename);
            Resource resource = new UrlResource(file.toUri());
            if (resource.exists() || resource.isReadable()) {
                return resource;
            } else {
                throw new StorageFileNotFoundException("Could not read file: " + filename);

            }
        } catch (MalformedURLException e) {
            throw new StorageFileNotFoundException("Could not read file: " + filename, e);
        }
    }

    @Override
    public void delete(String folder, String filename) {
        Path path = Paths.get(uploadPath + folder + filename);
        try {
            Files.delete(path);
        } catch (NoSuchFileException ex) {
            System.out.printf("No such file or directory: %s\n", path);
        } catch (DirectoryNotEmptyException ex) {
            System.out.printf("Directory %s is not empty\n", path);
        } catch (IOException ex) {
            System.out.println(ex);
        }
    }
}
