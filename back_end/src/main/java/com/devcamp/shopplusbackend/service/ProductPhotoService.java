package com.devcamp.shopplusbackend.service;

import com.devcamp.shopplusbackend.entity.Product;
import com.devcamp.shopplusbackend.entity.ProductPhoto;
import com.devcamp.shopplusbackend.model.ResponseFile;
import com.devcamp.shopplusbackend.repository.ProductPhotoRepository;
import com.devcamp.shopplusbackend.repository.ProductRepository;
import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class ProductPhotoService {

    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private ProductPhotoRepository productPhotoRepository;

    @Autowired
    private StorageService storageService;

    public List<ResponseFile> storeProductPhotos(Long productId, MultipartFile[] files) {
        List<ResponseFile> responseFiles = new ArrayList<>();
        Optional<Product> optionalProduct = productRepository.findById(productId);
        if (optionalProduct.isPresent()) {
            for (MultipartFile file : files) {
                ProductPhoto productPhoto = new ProductPhoto();
                productPhoto.setProduct(optionalProduct.get());
                String fileType = FilenameUtils.getExtension(file.getOriginalFilename());
                productPhoto.setName(UUID.randomUUID() + "." + fileType);
                ProductPhoto photo = productPhotoRepository.save(productPhoto);
                storageService.store("/product-photos/", photo.getName(), file);
                ResponseFile responseFile = new ResponseFile();
                responseFile.setId(photo.getId());
                responseFile.setName(photo.getName());
                responseFile.setUrl(ServletUriComponentsBuilder.fromCurrentContextPath().path("/product-photos/").path(photo.getName()).toUriString());
                responseFiles.add(responseFile);
            }
            return responseFiles;
        }
        return null;
    }

    public List<Long> deleteProductPhotos(List<Long> ids) throws IOException {
        List<Long> deletedIds = new ArrayList<>();
        for (Long id : ids) {
            Optional<ProductPhoto> optionalProductPhoto = productPhotoRepository.findById(id);
            if (optionalProductPhoto.isPresent()) {
                storageService.delete("/product-photos/", optionalProductPhoto.get().getName());
                productPhotoRepository.deleteById(id);
                deletedIds.add(id);
            }
        }
        return deletedIds;
    }
}
