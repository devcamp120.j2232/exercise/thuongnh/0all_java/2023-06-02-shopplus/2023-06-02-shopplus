package com.devcamp.shopplusbackend.controller;

import com.devcamp.shopplusbackend.entity.ProductLine;
import com.devcamp.shopplusbackend.model.Result;
import com.devcamp.shopplusbackend.service.ProductLineService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

@CrossOrigin
@RestController
public class ProductLineController {
    private static final Logger logger = LoggerFactory.getLogger(ProductLineController.class);

    @Autowired
    private ProductLineService productLineService;

    @GetMapping("/product-lines")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<Result> getProductLines(@RequestParam(defaultValue = "") String keyword, @RequestParam(defaultValue = "0") int page, @RequestParam(defaultValue = "10") int size) {
        try {
            Page<ProductLine> productLines = productLineService.getProductLines(keyword, page, size);
            Result result = new Result(productLines.getTotalPages(), productLines.getNumberOfElements(), productLines.getTotalElements(), productLines.getContent());
            return new ResponseEntity<>(result, HttpStatus.OK);
        } catch (Exception e) {
            logger.error("Cannot get productLines: {}", e.getMessage());
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/product-lines/{id}")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<ProductLine> getProductLineById(@PathVariable Long id) {
        try {
            ProductLine productLine = productLineService.getProductLineById(id);
            if (productLine != null) {
                return new ResponseEntity<>(productLine, HttpStatus.OK);
            }
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            logger.error("Cannot get productLine: {}", e.getMessage());
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/product-lines")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<ProductLine> createProductLine(@RequestBody ProductLine pProductLine) {
        try {
            ProductLine productLine = productLineService.createProductLine(pProductLine);
            return new ResponseEntity<>(productLine, HttpStatus.CREATED);
        } catch (Exception e) {
            logger.error("Failed to Create specified ProductLine: {}", e.getMessage());
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/product-lines/{id}")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<Object> updateProductLine(@PathVariable Long id, @RequestBody ProductLine pProductLine) {
        try {
            ProductLine productLine = productLineService.updateProductLine(id, pProductLine);
            if (productLine != null) {
                return new ResponseEntity<>(productLine, HttpStatus.OK);
            }
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            logger.error("Failed to Update specified ProductLine: {}", e.getMessage());
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/product-lines/{id}")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<ProductLine> deleteProductLine(@PathVariable Long id) {
        try {
            boolean isDeleted = productLineService.deleteProductLine(id);
            if (isDeleted) {
                return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
            }
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            logger.error("Failed to Delete specified ProductLine: {}", e.getMessage());
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/product-lines/name/{name}/exists")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<Boolean> checkByName(@PathVariable String name) {
        try {
            boolean check = productLineService.existsByName(name);
            return new ResponseEntity<>(check, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
