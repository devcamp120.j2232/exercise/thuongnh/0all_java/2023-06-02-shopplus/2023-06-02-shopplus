package com.devcamp.shopplusbackend.controller;

import com.devcamp.shopplusbackend.entity.Graphics;
import com.devcamp.shopplusbackend.entity.GraphicsType;
import com.devcamp.shopplusbackend.model.Result;
import com.devcamp.shopplusbackend.service.GraphicsTypeService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.Set;

@CrossOrigin
@RestController
public class GraphicsTypeController {
    private static final Logger logger = LoggerFactory.getLogger(GraphicsTypeController.class);

    @Autowired
    private GraphicsTypeService graphicsTypeService;

    @GetMapping("/graphics-types")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<Result> getGraphicsTypes(@RequestParam(defaultValue = "") String keyword, @RequestParam(defaultValue = "0") int page, @RequestParam(defaultValue = "10") int size) {
        try {
            Page<GraphicsType> graphicsTypes = graphicsTypeService.getGraphicsTypes(keyword, page, size);
            Result result = new Result(graphicsTypes.getTotalPages(), graphicsTypes.getNumberOfElements(), graphicsTypes.getTotalElements(), graphicsTypes.getContent());
            return new ResponseEntity<>(result, HttpStatus.OK);
        } catch (Exception e) {
            logger.error("Cannot get graphicsTypes: {}", e.getMessage());
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/graphics-types/{id}")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<GraphicsType> getGraphicsTypeById(@PathVariable Long id) {
        try {
            GraphicsType graphicsType = graphicsTypeService.getGraphicsTypeById(id);
            if (graphicsType != null) {
                return new ResponseEntity<>(graphicsType, HttpStatus.OK);
            }
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            logger.error("Cannot get graphicsType: {}", e.getMessage());
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/graphics-types/{id}/graphics")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<Set<Graphics>> getGraphics(@PathVariable Long id) {
        try {
            Set<Graphics> graphics = graphicsTypeService.getGraphics(id);
            if (graphics != null) {
                return new ResponseEntity<>(graphics, HttpStatus.OK);
            }
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            logger.error("Cannot get graphics: {}", e.getMessage());
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/graphics-types")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<GraphicsType> createGraphicsType(@RequestBody GraphicsType pGraphicsType) {
        try {
            GraphicsType graphicsType = graphicsTypeService.createGraphicsType(pGraphicsType);
            return new ResponseEntity<>(graphicsType, HttpStatus.CREATED);
        } catch (Exception e) {
            logger.error("Failed to Create specified GraphicsType: {}", e.getMessage());
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/graphics-types/{id}")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<Object> updateGraphicsType(@PathVariable Long id, @RequestBody GraphicsType pGraphicsType) {
        try {
            GraphicsType graphicsType = graphicsTypeService.updateGraphicsType(id, pGraphicsType);
            if (graphicsType != null) {
                return new ResponseEntity<>(graphicsType, HttpStatus.OK);
            }
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            logger.error("Failed to Update specified GraphicsType: {}", e.getMessage());
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/graphics-types/{id}")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<GraphicsType> deleteGraphicsType(@PathVariable Long id) {
        try {
            boolean isDeleted = graphicsTypeService.deleteGraphicsType(id);
            if (isDeleted) {
                return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
            }
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            logger.error("Failed to Delete specified GraphicsType: {}", e.getMessage());
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/graphics-types/name/{name}/exists")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<Boolean> checkByName(@PathVariable String name) {
        try {
            boolean check = graphicsTypeService.existsByName(name);
            return new ResponseEntity<>(check, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
