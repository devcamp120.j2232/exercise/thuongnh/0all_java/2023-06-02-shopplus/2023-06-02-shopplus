package com.devcamp.shopplusbackend.controller;

import com.devcamp.shopplusbackend.entity.Token;
import com.devcamp.shopplusbackend.entity.User;
import com.devcamp.shopplusbackend.model.*;
import com.devcamp.shopplusbackend.security.JwtUtil;
import com.devcamp.shopplusbackend.security.UserPrincipal;
import com.devcamp.shopplusbackend.service.TokenService;
import com.devcamp.shopplusbackend.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.HashSet;
import java.util.Set;

@CrossOrigin
@RestController
// xác thực và quản lý người dùng. 
public class AuthController {
    private static final Logger logger = LoggerFactory.getLogger(AuthController.class);

    @Autowired
    private UserService userService;

    @Autowired
    private TokenService tokenService;

    @Autowired
    private PasswordEncoder encoder;

    @Autowired
    private JwtUtil jwtUtil;

    // Đăng ký tài khoản
    @PostMapping("/auth/register")
    public ResponseEntity<User> register(@RequestBody RequestUser requestUser) {
        try {
            // Gọi service để đăng ký tài khoản người dùng
            User user = userService.registerUser(requestUser);
            if (user != null) {
                // Trả về ứng dụng mã HTTP 201 CREATED nếu đăng ký thành công
                return new ResponseEntity<>(user, HttpStatus.CREATED);
            } else {
                // Trả về ứng dụng mã HTTP 422 UNPROCESSABLE_ENTITY nếu đăng ký không thành công
                return new ResponseEntity<>(null, HttpStatus.UNPROCESSABLE_ENTITY);
            }
        } catch (Exception e) {
            // Ghi log lỗi và trả về ứng dụng mã HTTP 500 INTERNAL_SERVER_ERROR nếu xảy ra lỗi trong quá trình đăng ký
            logger.error("Failed to Register: {}", e.getMessage());
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // Đăng nhập
    @PostMapping("/auth/login")
    public ResponseEntity<LoginResult> login(@RequestBody RequestUser requestUser) {
        try {
            // Tìm kiếm thông tin người dùng dựa trên tên đăng nhập
            UserPrincipal userPrincipal = userService.findByUsername(requestUser.getUsername());
            if (null == userPrincipal || !encoder.matches(requestUser.getPassword(), userPrincipal.getPassword()) || !userPrincipal.isAccountNonLocked() || !userPrincipal.isEnabled()) {
                // Trả về ứng dụng mã HTTP 400 BAD_REQUEST nếu tên đăng nhập hoặc mật khẩu không chính xác, tài khoản bị khóa hoặc không được kích hoạt
                return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
            }
            // Tạo một token mới và lưu trữ nó trong cơ sở dữ liệu
            Token token = new Token();
            token.setToken(jwtUtil.generateToken(userPrincipal));
            token.setTokenExpDate(jwtUtil.generateExpirationDate());
            token.setCreatedBy(userPrincipal.getId());
            tokenService.createToken(token);
            // Lấy thông tin người dùng và vai trò của người dùng từ cơ sở dữ liệu
            User user = userService.getUserById(userPrincipal.getId());
            Set<String> strRoles = new HashSet<>();
            user.getRoles().forEach(role -> strRoles.add(role.getRoleKey()));
            // Trả về thông tin đăng nhập thành công và token cùng với vai trò của người dùng
            LoginResult loginResult = new LoginResult(token.getToken(), strRoles);
            return new ResponseEntity<>(loginResult, HttpStatus.OK);
        } catch (Exception e) {
            // Ghi log lỗi và trả về ứng dụng mã HTTP 500 INTERNAL_SERVER_ERROR nếu xảy ra lỗi trong quá trình đăng nhập
            logger.error("Failed to Login: {}", e.getMessage());
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // Lấy thông tin người dùng
    @GetMapping("/auth/user-info")
    @PreAuthorize("hasAnyRole('ADMIN', 'SELLER', 'CUSTOMER')")
    public ResponseEntity<UserInfo> getUserInfo() {
        try {
            // Lấy tên người dùng từ SecurityContextHolder
            String username = SecurityContextHolder.getContext().getAuthentication().getName();
            // Lấy thông tin người dùng dựa trên tên người dùng
            UserInfo userInfo = userService.getUserInfoByUsername(username);
            // Trả về thông tin người dùng thành công
            return new ResponseEntity<>(userInfo, HttpStatus.OK);
        } catch (Exception e) {
            // Ghi log lỗi và trả về ứng dụng mã HTTP 500 INTERNAL_SERVER_ERROR nếu xảy ra lỗi trong quá trình lấy thông tin người dùng
            logger.error("Cannot get user info: {}", e.getMessage());
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // Chỉnh sửa thông tin người dùng
    @PostMapping("/auth/edit-profile")
    @PreAuthorize("hasAnyRole('ADMIN', 'SELLER', 'CUSTOMER')")
    public ResponseEntity<UserInfo> editProfile(@Valid @RequestBody RequestUser userEdit) {
        try {
            // Lấy tên người dùng từ SecurityContextHolder
            String username = SecurityContextHolder.getContext().getAuthentication().getName();
            // Cập nhật thông tin người dùng dựa trên tên người dùng và thông tin được cung cấp
            UserInfo userInfo = userService.updateProfile(username, userEdit);
            if (userInfo != null) {
                // Trả về ứng dụng mã HTTP 201 CREATED nếu chỉnh sửa thông tin thành công
                return new ResponseEntity<>(userInfo, HttpStatus.CREATED);
            } else {
                // Trả về ứng dụng mã HTTP 422 UNPROCESSABLE_ENTITY nếu chỉnh sửa thông tin không thành công
                return new ResponseEntity<>(null, HttpStatus.UNPROCESSABLE_ENTITY);
            }
        } catch (Exception e) {
            // Ghi log lỗi và trả về ứng dụng mã HTTP 500 INTERNAL_SERVER_ERROR nếu xảy ra lỗi trong quá trình chỉnh sửa thông tin người dùng
            logger.error("Failed to Register: {}", e.getMessage());
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // Đổi mật khẩu
    @PutMapping("/auth/change-password")
    @PreAuthorize("hasAnyRole('ADMIN', 'SELLER', 'CUSTOMER')")
    public ResponseEntity<Boolean> changePassword(@RequestParam String currentPassword, @RequestParam String newPassword) {
        try {
            // Lấy tên người dùng từ SecurityContextHolder
            String username = SecurityContextHolder.getContext().getAuthentication().getName();
            // Thay đổi mật khẩu người dùng dựa trên tên người dùng, mật khẩu hiện tại và mật khẩu mới được cung cấp
            boolean isChanged = userService.changePassword(username, currentPassword, newPassword);
            if (isChanged) {
                // Nếu mật khẩu đã được thay đổi thành công, xóa token của người dùng khỏi cơ sở dữ liệu
                tokenService.deleteTokenByCreatedBy(userService.findByUsername(username).getId());
            }
            // Trả về kết quả thành công hoặc thất bại của việc thay đổi mật khẩu
            return new ResponseEntity<>(isChanged, HttpStatus.OK);
        } catch (Exception e) {
            // Trả về ứng dụng mã HTTP 500 INTERNAL_SERVER_ERROR nếu xảy ra lỗi trong quá trình thay đổi mật khẩu
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
