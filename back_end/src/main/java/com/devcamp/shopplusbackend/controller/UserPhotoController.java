package com.devcamp.shopplusbackend.controller;


import com.devcamp.shopplusbackend.entity.UserPhoto;
import com.devcamp.shopplusbackend.model.ResponseFile;
import com.devcamp.shopplusbackend.service.StorageService;
import com.devcamp.shopplusbackend.service.UserPhotoService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

@CrossOrigin
@RestController
public class UserPhotoController {
    private static final Logger logger = LoggerFactory.getLogger(UserPhotoController.class);

    @Autowired
    private UserPhotoService userPhotoService;

    @Autowired
    private StorageService storageService;

    @GetMapping("/user-photos/{filename}")
    public ResponseEntity<Resource> downloadUserPhoto(@PathVariable String filename) {
        try {
            Resource resource = storageService.loadAsResource("/user-photos/", filename);
            return ResponseEntity.ok().header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + filename + "\"").body(resource);
        } catch (Exception e) {
            logger.error("Failed to download file: {}", e.getMessage());
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/user-photos")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<ResponseFile> uploadUserPhoto(@RequestParam Long userId, @RequestParam("file") MultipartFile file) {
        try {
            ResponseFile responseFile = userPhotoService.storeUserPhoto(userId, file);
            if (responseFile != null) {
                return new ResponseEntity<>(responseFile, HttpStatus.OK);
            }
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            logger.error("Failed to upload file: {}", e.getMessage());
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/user-photos/{id}")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<Long> deleteUserPhoto(@PathVariable Long id) {
        try {
            Long deletedId = userPhotoService.deleteUserPhoto(id);
            return new ResponseEntity<>(deletedId, HttpStatus.OK);
        } catch (Exception e) {
            logger.error("Failed to Delete specified UserPhoto: {}", e.getMessage());
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
