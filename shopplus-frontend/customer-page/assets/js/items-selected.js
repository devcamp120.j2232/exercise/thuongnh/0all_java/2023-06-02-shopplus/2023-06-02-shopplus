"use strict";
/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */

/*** REGION 2 - Vùng gán / thực thi sự kiện cho các elements */
onPageLoading();

//Gán sự kiện cho nút View Cart
$("#view-cart").on("mouseover", loadDataToViewCart);

//Gán sự kiện cho nút View Wishlist
$("#view-wishlist").on("mouseover", loadDataToWishList);

//Gán sự kiện cho nút Remove All Wishlist
$("#remove-all-wishlist").on("click", onRemoveAllWishlistClick);

//Gán sự kiện cho nút Remove this item
$("#ul-list-cart-items").on("click", ".remove-item", function () {
    onRemoveSelectedItemClick($(this));
});

$("#ul-wishlist-items").on("click", ".remove-item", function () {
    onRemoveWishListItemClick($(this));
});

/*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
function onPageLoading() {
    "use strict";
    loadDataToViewCart();
    loadDataToWishList();
}

//Hàm xử lý mouseover View Cart
function loadDataToViewCart() {
    "use strict";
    $("#ul-list-cart-items").html("");
    var vOrderDetails = getCookie("order_details") ? JSON.parse(getCookie("order_details")) : "";
    var vTotalItems = 0;
    var vTotalAmount = 0;
    if (vOrderDetails != null && vOrderDetails != "") {
        vOrderDetails.forEach(element => {
            var vProduct = getSelectedProductData(element.productId);
            if (vProduct != null) {
                vTotalItems += parseInt(element.quantityOrder);
                vTotalAmount += element.quantityOrder * vProduct.priceEach;
                var vImgSrc = vProduct.photoName != "" ? gBASE_URL + "product-photos/" + vProduct.photoName : "./assets/images/no-image.png";
                var vText = ""
                    + "<li>"
                    + "<a href='javascript:void(0)' data-product-id='" + element.productId + "' class='remove remove-item' title='Remove this item'><i class='fa-solid fa-xmark'></i></a>"
                    + "<div class='cart-img-head'>"
                    + "<a class='cart-img' href='product-details.html?id=" + element.productId + "'><img src='" + vImgSrc + "' alt='#'></a>"
                    + "</div>"
                    + "<div class='content'>"
                    + "<h4><a href='product-details.html?id=" + element.productId + "'>" + vProduct.fullName + "</a></h4>"
                    + "<p class='quantity'>" + element.quantityOrder + "x - <span class='amount'>$" + vProduct.priceEach.toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") + "</span></p>"
                    + "</div>"
                    + "</li>";
                $("#ul-list-cart-items").append(vText);
            }
        });
    }
    $("#total-cart-items-1").html(vTotalItems);
    $("#total-cart-items-2").html(vTotalItems + " Items");
    $("#total-amount").html("$" + vTotalAmount.toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","));
}

//Hàm xử lý mouseover WishList
function loadDataToWishList() {
    "use strict";
    $("#ul-wishlist-items").html("");
    var vListProducts = getCookie("wishlist") ? JSON.parse(getCookie("wishlist")) : "";
    var vTotalItems = 0;
    if (vListProducts != null && vListProducts != "") {
        vListProducts.forEach(element => {
            var vProduct = getSelectedProductData(element);
            if (vProduct != null) {
                vTotalItems++;
                var vImgSrc = vProduct.photoName != "" ? gBASE_URL + "product-photos/" + vProduct.photoName : "./assets/images/no-image.png";
                var vText = ""
                    + "<li>"
                    + "<a href='javascript:void(0)' data-product-id='" + element + "' class='remove remove-item' title='Remove this item'><i class='fa-solid fa-xmark'></i></a>"
                    + "<div class='wishlist-img-head'>"
                    + "<a class='wishlist-img' href='product-details.html?id=" + element + "'><img src='" + vImgSrc + "' alt='#'></a>"
                    + "</div>"
                    + "<div class='content'>"
                    + "<h4><a href='product-details.html?id=" + element + "'>" + vProduct.fullName + "</a></h4>"
                    + "<p class='quantity'>$" + vProduct.priceEach.toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") + "<span class='amount'>$" + vProduct.buyPrice.toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") + "</span></p>"
                    + "</div>"
                    + "</li>";
                $("#ul-wishlist-items").append(vText);
            }
        });
    }
    $("#total-wishlist-items-1").html(vTotalItems);
    $("#total-wishlist-items-2").html(vTotalItems + " Items");
}

//Hàm xử lý khi ấn nút Remove All Wishlist
function onRemoveAllWishlistClick() {
    "use strict";
    setCookie("wishlist", "", 10, false);
    loadDataToWishList();
}

//Hàm xử lý khi ấn nút Remove this item
function onRemoveSelectedItemClick(paramElement) {
    "use strict";
    var vProductId = paramElement[0].dataset.productId;
    var vOrderDetails = getCookie("order_details") ? JSON.parse(getCookie("order_details")) : "";
    vOrderDetails.forEach((element, index) => {
        if (vProductId == element.productId) {
            vOrderDetails.splice(index, 1);
        }
    });
    var vOrderDetailsJson = JSON.stringify(vOrderDetails);
    setCookie("order_details", vOrderDetailsJson, 1, true);
    loadDataToViewCart();
}

function onRemoveWishListItemClick(paramElement) {
    "use strict";
    var vProductId = paramElement[0].dataset.productId;
    var vOrderDetails = getCookie("wishlist") ? JSON.parse(getCookie("wishlist")) : "";
    vOrderDetails.forEach((element, index) => {
        if (vProductId == element) {
            vOrderDetails.splice(index, 1);
        }
    });
    var vOrderDetailsJson = JSON.stringify(vOrderDetails);
    setCookie("wishlist", vOrderDetailsJson, 10, true);
    loadDataToWishList();
}

/*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
//Hàm lấy dữ liệu product
function getSelectedProductData(paramId) {
    "use strict";
    var vProduct = {
        fullName: "",
        buyPrice: "",
        priceEach: "",
        photoName: ""
    }
    $.ajax({
        url: gBASE_URL + "products/" + paramId,
        method: "GET",
        async: false,
        success: function (res) {
            vProduct.fullName = res.fullName;
            vProduct.buyPrice = res.buyPrice;
            vProduct.priceEach = res.priceEach;
            var vProductPhotos = res.productPhotos;
            if (vProductPhotos != null && vProductPhotos.length > 0) {
                vProduct.photoName = vProductPhotos[0].name;
            } else {
                vProduct.photoName = "";
            }
        }, error: function (err) {
            vProduct = null;
            console.log(err.responseText);
        }
    });
    return vProduct;
}
