$(document).ready(function () {
    "use strict";
    /*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
    var gPage = 0;
    var gTotalPages = 0;
    var gOrderCode = "";
    var gOrderStatus = "";

    /*** REGION 2 - Vùng gán / thực thi sự kiện cho các elements */
    onPageLoading();

    //Gán sự kiện cho nút Edit Profile
    $("#btn-edit-profile").on("click", onBtnEditProfileClick);

    //Gán sự kiện cho nút Save Profile
    $("#btn-save-profile").on("click", onBtnSaveProfileClick);

    //Gán sự kiện cho nút View more
    $("#btn-view-more").on("click", onBtnViewMoreClick);

    //Gán sự kiện click Order Code
    $("#table-orders").on("click", ".order-code", function () {
        onOrderCodeClick($(this));
    });

    //Gán sự kiện click Cancel Order trên modal Order Detail
    $("#order-detail-modal").on("click", ".btn-cancel", function () {
        $("#order-detail-modal").modal("hide");
        $("#cancel-order-modal").modal("show");
    });

    //Gán sự kiện click Cancel Order Confirm
    $("#cancel-order-confirm").on("click", onBtnCancelOrderConfirmClick);

    /*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
    function onPageLoading() {
        const token = getCookie("token");
        if (token) {
            $.ajax({
                url: gBASE_URL + "auth/user-info",
                method: "GET",
                headers: {
                    Authorization: "Bearer " + token
                },
                success: function (res) {
                    $("#p-fullName").html(res.firstName + " " + res.lastName);
                    $("#p-phoneNumber").html(res.username);
                    $("#p-address").html(res.address);
                    $("#p-city").html(res.city);
                    $("#p-state").html(res.state);
                    $("#p-postalCode").html(res.postalCode);
                    $("#p-country").html(res.country);
                },
                error: function (err) {
                    console.log(err);
                }
            });
        } else {
            window.location.href = "login.html";
        }
        getOrderData(gPage);
    }

    //Hàm xử lý khi ấn nút Edit Profile
    function onBtnEditProfileClick() {
        "use strict";
        const token = getCookie("token");
        if (token) {
            $.ajax({
                url: gBASE_URL + "auth/user-info",
                method: "GET",
                headers: {
                    Authorization: "Bearer " + token
                },
                success: function (res) {
                    $("#inp-firstName").val(res.firstName);
                    $("#inp-lastName").val(res.lastName);
                    $("#inp-address").val(res.address);
                    $("#inp-city").val(res.city);
                    $("#inp-state").val(res.state);
                    $("#inp-postalCode").val(res.postalCode);
                    $("#inp-country").val(res.country);
                    $("#edit-profile-modal").modal("show");
                },
                error: function (err) {
                    console.log(err);
                }
            });
        } else {
            window.location.href = "login.html";
        }
    }

    //Hàm xử lý khi ấn nút Save Profile
    function onBtnSaveProfileClick() {
        "use strict";
        const token = getCookie("token");
        var vUseEdit = {
            firstName: "",
            lastName: "",
            address: "",
            city: "",
            state: "",
            postalCode: "",
            country: ""
        }
        getProfileData(vUseEdit);
        var vIsCheck = validateProfileData(vUseEdit);
        if (token) {
            if (vIsCheck) {
                $.ajax({
                    url: gBASE_URL + "auth/edit-profile",
                    method: "POST",
                    headers: {
                        Authorization: "Bearer " + token,
                        "Content-Type": "application/json"
                    },
                    data: JSON.stringify(vUseEdit),
                    success: function (res) {
                        location.reload();
                    },
                    error: function (err) {
                        console.log(err);
                    }
                });
            }
        } else {
            window.location.href = "login.html";
        }
    }

    //Hàm xử lý khi ấn View more
    function onBtnViewMoreClick() {
        "use strict";
        if (gPage <= gTotalPages - 1) {
            gPage++;
            getOrderData(gPage);
        } else {
            $("#btn-view-more").prop("hidden", true);
        }
    }

    //Hàm xử lý khi ấn vào Order Code
    function onOrderCodeClick(paramElement) {
        "use strict";
        gOrderCode = paramElement[0].dataset.orderCode;
        gOrderStatus = paramElement[0].dataset.orderStatus;
        $("#modal-title-order-code").html(gOrderCode);
        const token = getCookie("token");
        if (token) {
            $.ajax({
                url: gBASE_URL + "orders/" + gOrderCode + "/order-by-user",
                method: "GET",
                headers: {
                    Authorization: "Bearer " + token
                },
                success: function (res) {
                    $("#contact-name").html(res.firstName + " " + res.lastName);
                    $("#contact-phoneNumber").html(res.phoneNumber);
                    $("#contact-address").html(res.address);
                    $("#contact-city").html(res.city);
                    $("#contact-state").html(res.state);
                    $("#contact-postalCode").html(res.postalCode);
                    $("#contact-country").html(res.country);
                },
                error: function (err) {
                    console.log(err);
                }
            });
            $.ajax({
                url: gBASE_URL + "orders/" + gOrderCode + "/order-details-by-user",
                method: "GET",
                headers: {
                    Authorization: "Bearer " + token
                },
                success: function (res) {
                    loadOrderDetailDataToTable(res);
                    if (gOrderStatus == "Pending") {
                        $("#order-detail-modal .modal-footer").prop("hidden", false);
                    } else {
                        $("#order-detail-modal .modal-footer").prop("hidden", true);
                    }
                    $("#order-detail-modal").modal("show");
                },
                error: function (err) {
                    console.log(err);
                }
            });
        } else {
            window.location.href = "login.html";
        }
    }

    //Hàm xử lý khi click Cancel Order Confirm
    function onBtnCancelOrderConfirmClick() {
        "use strict";
        const token = getCookie("token");
        if (token) {
            $.ajax({
                url: gBASE_URL + "orders/" + gOrderCode + "/delete-by-user",
                method: "DELETE",
                headers: {
                    Authorization: "Bearer " + token
                },
                success: function (res) {
                    $("#tbody-orders").html("");
                    for (var bI = 0; bI <= gPage; bI++) {
                        getOrderData(bI);
                    }
                    $("#cancel-order-modal").modal("hide");
                },
                error: function (err) {
                    console.log(err);
                }
            });
        } else {
            window.location.href = "login.html";
        }
    }

    /*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
    //Hàm lấy dữ liệu profile
    function getProfileData(paramUserEdit) {
        "use strict"
        paramUserEdit.firstName = $("#inp-firstName").val().trim();
        paramUserEdit.lastName = $("#inp-lastName").val().trim();
        paramUserEdit.address = $("#inp-address").val().trim();
        paramUserEdit.city = $("#inp-city").val().trim();
        paramUserEdit.state = $("#inp-state").val().trim();
        paramUserEdit.postalCode = $("#inp-postalCode").val().trim();
        paramUserEdit.country = $("#inp-country").val().trim();
    }

    //Hàm lấy kiểm tra dữ liệu profile
    function validateProfileData(paramUserEdit) {
        "use strict"
        if (paramUserEdit.firstName == "") {
            toastr.warning("Enter your first name, please!");
            return false;
        }
        if (paramUserEdit.lastName == "") {
            toastr.warning("Enter your last name, please!");
            return false;
        }
        return true;
    }

    //Hàm lấy dữ liệu Orders
    function getOrderData(paramPage) {
        "use strict";
        const token = getCookie("token");
        if (token) {
            $.ajax({
                url: gBASE_URL + "orders/orders-by-user?page=" + paramPage,
                method: "GET",
                headers: {
                    Authorization: "Bearer " + token
                },
                success: function (res) {
                    $("#p-totalOrders").html(`Orders (${res.totalElements})`);
                    gTotalPages = res.totalPages;
                    if (gTotalPages > 0) {
                        loadOrderDataTotable(res.content);
                    } else {
                        $("#btn-view-more").prop("hidden", true);
                    }
                },
                error: function (err) {
                    console.log(err);
                }
            });
        } else {
            window.location.href = "login.html";
        }
    }

    //Hàm load dữ liệu Orders
    function loadOrderDataTotable(paramData) {
        "use strict";
        var vText = "";
        paramData.forEach(element => {
            vText +=
                `<tr>
                    <td><a href="javascript:void(0)" class="order-code" data-order-code="${element.orderCode}" data-order-status="${element.status}">${element.orderCode}</a></td>
                    <td>${element.orderDate}</td>
                    <td>${element.requiredDate}</td>
                    <td>${(element.shippedDate != null ? element.shippedDate : "")}</td>
                    <td>$${element.totalOrderAmount.toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")}</td>
                    <td>${element.status}</td>
                </tr>`
        });
        $("#tbody-orders").append(vText);
        if (gPage >= gTotalPages - 1) {
            $("#btn-view-more").prop("hidden", true);
        }
    }

    //Hàm load dữ liệu Order Detail
    function loadOrderDetailDataToTable(paramData) {
        "use strict";
        var vText = "";
        paramData.forEach(element => {
            vText +=
                `<tr>
                    <td>${element.product.fullName}</td>
                    <td>${element.quantityOrder}</td>
                    <td>$${element.priceEach.toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")}</td>
                    <td>$${(element.priceEach * element.quantityOrder).toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")}</td>
                </tr>`
        });
        $("#tbody-order-details").html(vText);
    }
});