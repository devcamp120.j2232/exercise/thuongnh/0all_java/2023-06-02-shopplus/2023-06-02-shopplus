$(document).ready(function () {
    "use strict";
    /*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */

    /*** REGION 2 - Vùng gán / thực thi sự kiện cho các elements */
    onPageLoading();

    //Kiểm tra dữ liệu nhập vào trên form login
    $("#form-login").validate({
        submitHandler: function () {
            loginFunction();
        },
        rules: {
            inp_phoneNumber: {
                required: true
            },
            inp_password: {
                required: true,
                minlength: 6
            }
        },
        messages: {
            inp_phoneNumber: {
                required: "Please provide a Phone Number.",
            },
            inp_password: {
                required: "Please provide a Password.",
                minlength: "Password must has at least 6 characters."
            }
        },
        errorElement: 'span',
        errorClass: 'invalid-feedback',
        highlight: function (element, errorClass, validClass) {
            $(element).addClass('is-invalid');
        },
        unhighlight: function (element, errorClass, validClass) {
            $(element).removeClass('is-invalid');
        }
    });

    /*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
    function onPageLoading() {
        "use strict";
        checkCookie();
        if (sessionStorage.getItem("registerSuccess")) {
            if (sessionStorage.getItem("registerSuccess") == 1) {
                toastr.success("Account was created successfully.");
            }
            sessionStorage.removeItem("registerSuccess");
        }
        if (sessionStorage.getItem("changePasswordSuccess")) {
            if (sessionStorage.getItem("changePasswordSuccess") == 1) {
                toastr.success("Password has been changed.");
            }
            sessionStorage.removeItem("changePasswordSuccess");
        }
    }

    //Hàm xử lý Đăng nhập
    function loginFunction() {
        "use strict";
        var vLoginData = {
            username: $("#inp-phoneNumber").val().trim(),
            password: $("#inp-password").val().trim()
        }
        var vRemember = $("#check-remember").is(":checked");
        $.ajax({
            url: gBASE_URL + "auth/login",
            method: "POST",
            headers: {
                "Content-Type": "application/json"
            },
            data: JSON.stringify(vLoginData),
            success: function (res) {
                setCookie("token", res.token, 10, vRemember);
                window.location.href = "index.html";
            },
            error: function (err) {
                toastr.error("Login failed!");
                console.log(err.responseText);
            }
        });
    }

    /*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
    //Hàm setCookie
    function setCookie(cname, cvalue, exdays, remeber) {
        "use strict";
        var d = new Date();
        d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
        var expires = "expires=" + d.toUTCString();
        if (remeber) {
            document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
        } else {
            document.cookie = cname + "=" + cvalue + ";path=/";
        }
    }

    //Hàm get Cookie
    function getCookie(cname) {
        "use strict";
        var name = cname + "=";
        var decodedCookie = decodeURIComponent(document.cookie);
        var ca = decodedCookie.split(';');
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') {
                c = c.substring(1);
            }
            if (c.indexOf(name) == 0) {
                return c.substring(name.length, c.length);
            }
        }
        return "";
    }

    //Hàm kiểm tra cookie
    function checkCookie() {
        "use strict";
        const token = getCookie("token");
        if (token) {
            window.location.href = "index.html";
        }
    }
});