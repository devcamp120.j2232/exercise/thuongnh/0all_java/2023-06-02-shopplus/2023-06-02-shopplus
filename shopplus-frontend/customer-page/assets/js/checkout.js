"use strict";
/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */

/*** REGION 2 - Vùng gán / thực thi sự kiện cho các elements */
onPageLoading();

//Gán sự kiện cho nút Remove this item
$("#ul-list-cart-items").on("click", ".remove-item", function () {
    setTimeout(loadDataToPricingTable, 500);
});

//Gán sự kiện cho nút Order
$("#btn-order").on("click", onBtnOrderClick);

/*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
function onPageLoading() {
    "use strict";
    loadDataToPricingTable();

    const token = getCookie("token");
    if (token) {
        $.ajax({
            url: gBASE_URL + "auth/user-info",
            method: "GET",
            headers: {
                Authorization: "Bearer " + token
            },
            success: function (res) {
                $("#inp-firstName").val(res.firstName);
                $("#inp-lastName").val(res.lastName);
                $("#inp-phoneNumber").val(res.username);
                $("#inp-address").val(res.address);
                $("#inp-city").val(res.city);
                $("#inp-state").val(res.state);
                $("#inp-postalCode").val(res.postalCode);
                $("#inp-country").val(res.country);
            },
            error: function (err) {
                console.log(err);
            }
        });
    } else {
        window.location.href = "login.html";
    }
}

//Hàm xử lý khi ấn nút Order
function onBtnOrderClick() {
    "use strict";
    var token = getCookie("token");
    if (token) {
        //Lấy dữ liệu
        var vOrderDetails = getCookie("order_details");
        if (vOrderDetails != null && vOrderDetails != "" && vOrderDetails != "[]") {
            var vOrder = {
                firstName: "",
                lastName: "",
                phoneNumber: "",
                address: "",
                city: "",
                state: "",
                postalCode: "",
                country: ""
            }
            getOrderData(vOrder);
            var vIsCheckOrder = validateOrderData(vOrder);
            if (vIsCheckOrder) {
                var vRequestOrder = {
                    order: vOrder,
                    productsSelected: JSON.parse(vOrderDetails)
                }
                console.log(vRequestOrder);
                $.ajax({
                    url: gBASE_URL + "orders/create-order-by-user",
                    method: "POST",
                    async: false,
                    headers: {
                        Authorization: "Bearer " + token,
                        "Content-Type": "application/json"
                    },
                    data: JSON.stringify(vRequestOrder),
                    success: function (res) {
                        $("#b-orderCode").html(res.orderCode);
                        $("#modal-order-success").modal("show");
                        setCookie("order_details", "", 1, false);
                        loadDataToPricingTable();
                        $("#total-cart-items-1").html(0);
                    },
                    error: function (err) {
                        console.log(err.responseText);
                    }
                });
            }
        } else {
            toastr.warning("There are no items to Order!");
        }
    } else {
        window.location.href = "login.html"
    }
}

//Hàm xử lý load dữ liệu vào Checkout
function loadDataToPricingTable() {
    "use strict";
    var vOrderDetails = getCookie("order_details") ? JSON.parse(getCookie("order_details")) : "";
    var vSubtotalPrice = 0;
    var vTotalPayable = 0;
    if (vOrderDetails != null && vOrderDetails != "") {
        vOrderDetails.forEach(element => {
            var vProduct = getProductData(element.productId);
            if (vProduct != null) {
                vSubtotalPrice += vProduct.buyPrice * parseInt(element.quantityOrder);
                vTotalPayable += vProduct.priceEach * parseInt(element.quantityOrder);
            }
        });
    }
    $("#p-subtotal-price").html("$" + vSubtotalPrice.toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","));
    $("#p-discount").html("$" + (vSubtotalPrice - vTotalPayable).toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","));
    $("#p-total-payable").html("$" + vTotalPayable.toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","));
}

/*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
//Hàm lấy dữ liệu Order
function getOrderData(paramData) {
    "use strict"
    paramData.firstName = $("#inp-firstName").val().trim();
    paramData.lastName = $("#inp-lastName").val().trim();
    paramData.phoneNumber = $("#inp-phoneNumber").val().trim();
    paramData.address = $("#inp-address").val().trim();
    paramData.city = $("#inp-city").val().trim();
    paramData.state = $("#inp-state").val().trim();
    paramData.postalCode = $("#inp-postalCode").val().trim();
    paramData.country = $("#inp-country").val().trim();
}

//Hàm lấy dữ liệu product
function getProductData(paramId) {
    "use strict";
    var vProduct = {
        buyPrice: "",
        priceEach: ""
    }
    $.ajax({
        url: gBASE_URL + "products/" + paramId,
        method: "GET",
        async: false,
        success: function (res) {
            vProduct.buyPrice = res.buyPrice;
            vProduct.priceEach = res.priceEach;
        }, error: function (err) {
            vProduct = null;
            console.log(err.responseText);
        }
    });
    return vProduct;
}

//Hàm kiểm tra dữ liệu
function validateOrderData(paramData) {
    "use strict";
    if (paramData.firstName == "") {
        toastr.warning("Enter first name, please!");
        return false;
    }
    if (paramData.lastName == "") {
        toastr.warning("Enter last name, please!");
        return false;
    }
    if (paramData.phoneNumber == "") {
        toastr.warning("Enter phone number, please!");
        return false;
    }
    if (paramData.address == "") {
        toastr.warning("Enter address, please!");
        return false;
    }
    return true;
}