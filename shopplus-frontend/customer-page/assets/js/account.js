$(document).ready(function () {
    "use strict";
    /*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */

    /*** REGION 2 - Vùng gán / thực thi sự kiện cho các elements */

    //Gán sự kiện cho nút Profile
    $("#btn-profile").on("click", function () {
        window.location.href = "profile.html";
    });

    //Gán sự kiện cho nút Change Password
    $("#btn-change-password").on("click", function () {
        window.location.href = "change-password.html";
    });

    //Gán sự kiện cho nút Logout
    $("#btn-logout").on("click", onLogoutClick);

    /*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
    const token = getCookie("token");
    if (token) {
        $("#div-login-register").prop("hidden", true);
        $("#div-account").prop("hidden", false);
        $.ajax({
            url: gBASE_URL + "auth/user-info",
            method: "GET",
            headers: {
                Authorization: "Bearer " + token
            },
            success: function (res) {
                $("#btn-username").html(`<i class="fa-regular fa-user me-2"></i>${res.firstName} ${res.lastName}`);
            },
            error: function (err) {
                console.log(err);
            }
        });
    } else {
        $("#div-login-register").prop("hidden", false);
        $("#div-account").prop("hidden", true);
    }

    //Hàm xử lý khi ấn Đăng xuất
    function onLogoutClick() {
        "use strict";
        //Xóa cookie
        setCookie("token", "", 10, false);
        $("#div-login-register").prop("hidden", false);
        $("#div-account").prop("hidden", true);
    }

    /*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
});