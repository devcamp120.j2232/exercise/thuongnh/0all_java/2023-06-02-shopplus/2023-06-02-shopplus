"use strict";
/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */

/*** REGION 2 - Vùng gán / thực thi sự kiện cho các elements */
onPageLoading();

//Gán sự kiện cho nút Select quantity
$("#cart-single-list").on("change", ".sel-quantity", function () {
    onSelectQuantityChange($(this));
});

//Gán sự kiện cho nút Remove this item
$("#cart-single-list").on("click", ".remove-item", function () {
    onRemoveItemClick($(this));
});

//Gán sự kiện cho nút Remove this item
$("#ul-list-cart-items").on("click", ".remove-item", function () {
    setTimeout(loadDataToCart, 500);
});

/*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
function onPageLoading() {
    "use strict";
    loadDataToCart();
}

//Hàm load dữ liệu product
function loadDataToCart() {
    "use strict";
    $("#cart-single-list").html("");
    var vOrderDetails = getCookie("order_details") ? JSON.parse(getCookie("order_details")) : "";
    var vCartSubtotal = 0;
    var vPay = 0;
    if (vOrderDetails != null && vOrderDetails != "") {
        vOrderDetails.forEach(element => {
            var vProduct = getProductData(element.productId);
            if (vProduct != null) {
                vCartSubtotal += vProduct.buyPrice * parseInt(element.quantityOrder);
                vPay += vProduct.priceEach * parseInt(element.quantityOrder);
                var vImgSrc = vProduct.photoName != "" ? gBASE_URL + "product-photos/" + vProduct.photoName : "./assets/images/no-image.png";
                var vOption = "";
                for (var i = 1; i <= 5; i++) {
                    if (i == element.quantityOrder) {
                        vOption += "<option selected>" + i + "</option>";
                    } else {
                        vOption += "<option>" + i + "</option>";
                    }
                }
                var vSubtotal = "$" + (vProduct.buyPrice * element.quantityOrder).toFixed(2).toString().replace(/\B(?=(\d{3})(?!\d))/g, ",");
                var vDiscount = (vProduct.buyPrice - vProduct.priceEach > 0 ? "$" + ((vProduct.buyPrice - vProduct.priceEach) * element.quantityOrder).toFixed(2).toString().replace(/\B(?=(\d{3})(?!\d))/g, ",") : "_");
                var vText =
                    `<div class="cart-single-list">
                        <div class="row align-items-center">
                            <div class="col-lg-1 col-md-1 col-12">
                                <a href="product-details.html?id=${element.productId}"><img src="${vImgSrc}" alt="#"></a>
                            </div>
                            <div class="col-lg-4 col-md-3 col-12">
                                <h5 class="product-name"><a href="product-details.html?id=${element.productId}">${vProduct.fullName}</a>
                                </h5>
                                <p class="product-des">
                                    <span>${vProduct.cpu}</span>
                                    <span>${vProduct.memory}; ${vProduct.storage}</span>
                                </p>
                            </div>
                            <div class="col-lg-2 col-md-2 col-12">
                                <div class="count-input">
                                    <select data-product-id="${element.productId}" class="form-control sel-quantity">
                                        ${vOption}
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-2 col-md-2 col-12">
                                <p>${vSubtotal}</p>
                            </div>
                            <div class="col-lg-2 col-md-2 col-12">
                                <p>${vDiscount}</p>
                            </div>
                            <div class="col-lg-1 col-md-2 col-12">
                                <a data-product-id="${element.productId}" class="remove-item" href="javascript:void(0)"><i
                                        class="fa-solid fa-xmark"></i></a>
                            </div>
                        </div>
                    </div>`;
                $("#cart-single-list").append(vText);
            }
        });
    }
    $("#p-cart-subtotal").html("$" + vCartSubtotal.toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","));
    $("#p-save").html("$" + (vCartSubtotal - vPay).toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","));
    $("#p-pay").html("$" + vPay.toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","));
}

//Hàm xử lý SelectQuantityChange
function onSelectQuantityChange(paramElement) {
    "use strict";
    var vProductId = paramElement[0].dataset.productId;
    var vOrderDetails = getCookie("order_details") ? JSON.parse(getCookie("order_details")) : "";
    if (vOrderDetails == null || vOrderDetails == "") {
        vOrderDetails = [];
    }
    if (vProductId != "" && vProductId != null) {
        var vCount = 0;
        vOrderDetails.forEach((element, index) => {
            if (element.productId == vProductId) {
                vCount++;
                vOrderDetails[index] = {
                    productId: vProductId,
                    quantityOrder: paramElement.val()
                }
            }
        });
        if (vCount == 0) {
            var vOrderDetail = {
                productId: vProductId,
                quantityOrder: paramElement.val()
            }
            vOrderDetails.push(vOrderDetail);
        }
        var vOrderDetailsJson = JSON.stringify(vOrderDetails);
        setCookie("order_details", vOrderDetailsJson, 1, true);

        var vTotalItems = 0;
        vOrderDetails.forEach(element => {
            vTotalItems += parseInt(element.quantityOrder);
        });
        $("#total-cart-items-1").html(vTotalItems);
    }
    loadDataToCart();
}

//Hàm xử lý khi ấn nút Remove this item
function onRemoveItemClick(paramElement) {
    "use strict";
    var vProductId = paramElement[0].dataset.productId;
    var vOrderDetails = getCookie("order_details") ? JSON.parse(getCookie("order_details")) : "";
    vOrderDetails.forEach((element, index) => {
        if (vProductId == element.productId) {
            vOrderDetails.splice(index, 1);
        }
    });
    var vOrderDetailsJson = JSON.stringify(vOrderDetails);
    setCookie("order_details", vOrderDetailsJson, 1, true);
    loadDataToCart();

    var vTotalItems = 0;
    vOrderDetails.forEach(element => {
        vTotalItems += parseInt(element.quantityOrder);
    });
    $("#total-cart-items-1").html(vTotalItems);
}

/*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
//Hàm lấy dữ liệu product
function getProductData(paramId) {
    "use strict";
    var vProduct = {
        fullName: "",
        buyPrice: "",
        priceEach: "",
        cpu: "",
        memory: "",
        storage: "",
        photoName: ""
    }
    $.ajax({
        url: gBASE_URL + "products/" + paramId,
        method: "GET",
        async: false,
        success: function (res) {
            vProduct.fullName = res.fullName;
            vProduct.buyPrice = res.buyPrice;
            vProduct.priceEach = res.priceEach;
            vProduct.cpu = "CPU " + res.processor.fullName;
            vProduct.memory = "RAM " + res.systemMemory + " GB";
            vProduct.storage = res.storageType + " " + res.totalStorageCapacity + " GB";
            var vProductPhotos = res.productPhotos;
            if (vProductPhotos != null && vProductPhotos.length > 0) {
                vProduct.photoName = vProductPhotos[0].name;
            } else {
                vProduct.photoName = "";
            }
        }, error: function (err) {
            vProduct = null;
            console.log(err.responseText);
        }
    });
    return vProduct;
}
