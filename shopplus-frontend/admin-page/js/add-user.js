$(document).ready(function () {
    "use strict";
    /*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
    var gId = "";
    var gIsAdmin = false;

    /*** REGION 2 - Vùng gán / thực thi sự kiện cho các elements */
    onPageLoading();

    //Gán sự kiện click cho nút Add
    Array.from($("#form-add")).forEach(form => {
        form.addEventListener("submit", event => {
            event.preventDefault();
            event.stopPropagation();
            if (form.checkValidity()) {
                onBtnAddClick();
            }
            form.classList.add("was-validated");
        }, false);
    });

    //Gán sự kiện cho nút Cancel
    $("#btn-cancel").on("click", function () {
        window.history.back();
    });

    /*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
    function onPageLoading() {
        "use strict";

        $("#sel-roles").select2({
            theme: "bootstrap-5",
        });

        var token = getCookie("token");
        if (token) {
            $.ajax({
                url: gBASE_URL + "auth/user-info",
                method: "GET",
                headers: {
                    Authorization: "Bearer " + token
                },
                success: function (res) {
                    gIsAdmin = res.strRoles.includes("ROLE_ADMIN");
                    if (gIsAdmin) {
                        $("#hidden-password").prop("hidden", false);
                        $("#hidden-roles").prop("hidden", false);
                        $("#hidden-activated").prop("hidden", false);
                        $("#inp-password").prop("disabled", false);
                    }
                },
                error: function (err) {
                    console.log(err);
                }
            });
        } else {
            toastr.error("You are not logged in!");
        }
    }

    //Hàm xử lý khi ấn nút Add
    function onBtnAddClick() {
        "use strict";
        var token = getCookie("token");
        if (token) {
            var vUser = {
                firstName: "",
                lastName: "",
                username: "",
                password: "",
                address: "",
                city: "",
                state: "",
                postalCode: "",
                country: "",
                strRoles: "",
                activated: ""
            }
            //Lấy dữ liệu
            getUserData(vUser);
            var vUrl = "";
            if (gIsAdmin) {
                vUrl = gBASE_URL + "users/create-user-by-admin";
            } else {
                vUrl = gBASE_URL + "users/create-user-by-seller";
            }
            var vIsCheck = validateData(vUser);
            if (vIsCheck) {
                $.ajax({
                    url: vUrl,
                    method: "POST",
                    async: false,
                    headers: {
                        Authorization: "Bearer " + token,
                        "Content-Type": "application/json"
                    },
                    data: JSON.stringify(vUser),
                    success: function (res) {
                        window.location.href = "users.html";
                    },
                    error: function (err) {
                        console.log(err.responseText);
                    }
                });
            }
        } else {
            toastr.error("You are not logged in!");
        }
    }

    /*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
    //Hàm lấy dữ liệu User
    function getUserData(paramData) {
        "use strict";
        paramData.firstName = $("#inp-firstName").val().trim();
        paramData.lastName = $("#inp-lastName").val().trim();
        paramData.username = $("#inp-username").val().trim();
        paramData.password = $("#inp-password").val().trim();
        paramData.address = $("#inp-address").val().trim();
        paramData.city = $("#inp-city").val().trim();
        paramData.state = $("#inp-state").val().trim();
        paramData.postalCode = $("#inp-postalCode").val().trim();
        paramData.country = $("#inp-country").val().trim();
        paramData.strRoles = $("#sel-roles").val();
        paramData.activated = $("#cbx-activated").is(":checked");
    }

    //Hàm kiểm tra dữ liệu nhập vào
    function validateData(paramUserData) {
        if (checkUsername(paramUserData.username)) {
            toastr.warning("Username already exists!");
            return false;
        }
        return true;
    }

    //Hàm kiểm tra username
    function checkUsername(paramUsername) {
        "use strict";
        var vIsCheck = true;
        $.ajax({
            url: gBASE_URL + "users/username/" + paramUsername + "/exists",
            method: "GET",
            async: false,
            success: function (res) {
                vIsCheck = res;
            },
            error: function (err) {
                console.log(err.responseText);
            }
        });
        return vIsCheck;
    }
});