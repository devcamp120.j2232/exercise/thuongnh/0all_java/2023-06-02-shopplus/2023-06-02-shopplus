$(document).ready(function () {
    "use strict";
    /*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
    var gOrderDetails = [];

    /*** REGION 2 - Vùng gán / thực thi sự kiện cho các elements */
    onPageLoading();

    //Gán sự kiện click cho nút Add
    Array.from($("#form-add")).forEach(form => {
        form.addEventListener("submit", event => {
            event.preventDefault();
            event.stopPropagation();
            if (form.checkValidity()) {
                onBtnAddClick();
            }
            form.classList.add("was-validated");
        }, false);
    });

    //Gán sự kiện cho Select Customer
    $("#sel-customer").on("change", onSelectCustomerChange);

    //Gán sự kiện cho nút Add Customer
    $("#add-customer").on("click", function () {
        window.open("add-user.html", "_blank");
    });

    //Gán sự kiện cho nút Add Order Detail
    $("#btn-add-order-detail").on("click", function () {
        $("#sel-product").html("");
        $("#form-add-order-detail")[0].reset();
        $("#modal-add-order-detail").modal("show");
    });

    //Gán sự kiện cho nút Add trên modal Add Order Detail
    $("#form-add-order-detail").on("submit", function (event) {
        event.preventDefault();
        onBtnAddOrderDetailClick();
    });

    //Gán sự kiện cho nút Remove Order Detail
    $("#table-order-details").on("click", ".remove-order-detail", function () {
        onRemoveOrderDetailClick($(this));
    });

    //Gán sự kiện cho nút Cancel
    $("#btn-cancel").on("click", function () {
        window.history.back();
    });

    /*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
    function onPageLoading() {
        "use strict";
        var token = getCookie("token");
        if (token) {
            $("#sel-customer").select2({
                theme: "bootstrap-5",
                placeholder: "Enter Name / Phone Number",
                minimumInputLength: 2,
                allowClear: true,
                ajax: {
                    url: gBASE_URL + "users/select-search",
                    type: "GET",
                    headers: {
                        Authorization: "Bearer " + token
                    },
                    dataType: "json",
                    delay: 250,
                    data: function (params) {
                        return {
                            keyword: params.term
                        };
                    },
                    processResults: function (data, params) {
                        return {
                            results: $.map(data, function (user) {
                                return {
                                    text: user.firstName + " " + user.lastName + " - " + user.username,
                                    id: user.id,
                                    data: user
                                };
                            })
                        };
                    }
                }
            });

            $("#sel-product").select2({
                theme: "bootstrap-5",
                placeholder: "Enter Product Code / Name",
                minimumInputLength: 2,
                allowClear: true,
                dropdownParent: $("#modal-add-order-detail"),
                ajax: {
                    url: gBASE_URL + "products/select-search",
                    type: "GET",
                    headers: {
                        Authorization: "Bearer " + token
                    },
                    dataType: "json",
                    delay: 250,
                    data: function (params) {
                        return {
                            keyword: params.term
                        };
                    },
                    processResults: function (data, params) {
                        return {
                            results: $.map(data, function (product) {
                                return {
                                    text: product.fullName + " (" + product.productCode + ")",
                                    id: product.id,
                                    data: product
                                };
                            })
                        };
                    }
                }
            });
        } else {
            toastr.error("You are not logged in!");
        }
    }

    //Hàm xử lý khi chọn Customer
    function onSelectCustomerChange() {
        var vUserId = $("#sel-customer").val();
        if (vUserId != null) {
            var token = getCookie("token");
            if (token) {
                $.ajax({
                    url: gBASE_URL + "users/" + vUserId,
                    method: "GET",
                    headers: {
                        Authorization: "Bearer " + token
                    },
                    async: false,
                    success: function (res) {
                        $("#inp-firstName").val(res.firstName);
                        $("#inp-lastName").val(res.lastName);
                        $("#inp-phoneNumber").val(res.username);
                        $("#inp-address").val(res.address);
                        $("#inp-city").val(res.city);
                        $("#inp-state").val(res.state);
                        $("#inp-postalCode").val(res.postalCode);
                        $("#inp-country").val(res.country);
                    },
                    error: function (err) {
                        console.log(err.responseText);
                    }
                });
            } else {
                toastr.error("You are not logged in!");
            }
        }
    }

    //Hàm xử lý khi ấn nút Add
    function onBtnAddClick() {
        "use strict";
        var token = getCookie("token");
        if (token) {
            if (gOrderDetails.length > 0) {
                var vOrder = {
                    user: "",
                    requiredDate: "",
                    comments: "",
                    firstName: "",
                    lastName: "",
                    phoneNumber: "",
                    address: "",
                    city: "",
                    state: "",
                    postalCode: "",
                    country: ""
                }
                getOrderData(vOrder);
                var vRequestOrder = {
                    order: vOrder,
                    productsSelected: gOrderDetails
                }
                $.ajax({
                    url: gBASE_URL + "orders",
                    method: "POST",
                    async: false,
                    headers: {
                        Authorization: "Bearer " + token,
                        "Content-Type": "application/json"
                    },
                    data: JSON.stringify(vRequestOrder),
                    success: function (res) {
                        window.location.href = "orders.html";
                    },
                    error: function (err) {
                        console.log(err.responseText);
                    }
                });
            } else {
                toastr.warning("There are no order details to Add!");
            }
        } else {
            toastr.warning("You are not logged in!");
        }
    }

    // Hàm xử lý khi click nút Add trên modal Add Order Detail
    function onBtnAddOrderDetailClick() {
        "use strict";
        var vOrderDetail = {
            productId: "",
            productName: "",
            quantityOrder: "",
            priceEach: ""
        }
        var vQuantityOrder = $("#inp-quantityOrder").val();
        var vProduct = getProductData($("#sel-product").val());
        if (vProduct != null && vQuantityOrder <= vProduct.quantityInStock) {
            vOrderDetail.productId = vProduct.id;
            vOrderDetail.productName = vProduct.fullName;
            vOrderDetail.quantityOrder = vQuantityOrder;
            vOrderDetail.priceEach = vProduct.priceEach;
            var vIsElementExists = false;
            gOrderDetails.forEach(element => {
                if (vOrderDetail.productId == element.productId) {
                    vIsElementExists = true;
                    toastr.warning("Product already exists!");
                }
            });
            if (!vIsElementExists) {
                gOrderDetails.push(vOrderDetail);
                loadDataToOrderDetailsList();
                $("#modal-add-order-detail").modal("hide");
            }
        } else {
            toastr.warning("Product doesn't exist or there is not enough stock");
        }
    }

    //Hàm xử lý khi ấn nút Remove Order Detail
    function onRemoveOrderDetailClick(paramElement) {
        "use strict";
        var vProductId = paramElement[0].dataset.productId;
        gOrderDetails.forEach((element, index) => {
            if (vProductId == element.productId) {
                gOrderDetails.splice(index, 1);
            }
        });
        loadDataToOrderDetailsList();
    }

    /*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
    //Hàm lấy dữ liệu Order
    function getOrderData(paramData) {
        paramData.user = {
            id: $("#sel-customer").val()
        }
        paramData.requiredDate = $("#inp-requiredDate").val().trim();
        paramData.comments = $("#inp-comments").val().trim();
        paramData.firstName = $("#inp-firstName").val().trim();
        paramData.lastName = $("#inp-lastName").val().trim();
        paramData.phoneNumber = $("#inp-phoneNumber").val().trim();
        paramData.address = $("#inp-address").val().trim();
        paramData.city = $("#inp-city").val().trim();
        paramData.state = $("#inp-state").val().trim();
        paramData.postalCode = $("#inp-postalCode").val().trim();
        paramData.country = $("#inp-country").val().trim();
    }

    //Hàm lấy dữ liệu product
    function getProductData(paramId) {
        "use strict";
        var vProduct = {
            id: "",
            fullName: "",
            priceEach: "",
            quantityInStock: ""
        }
        var token = getCookie("token");
        if (token) {
            $.ajax({
                url: gBASE_URL + "products/" + paramId,
                method: "GET",
                headers: {
                    Authorization: "Bearer " + token
                },
                async: false,
                success: function (res) {
                    vProduct.id = res.id;
                    vProduct.fullName = res.fullName + " (" + res.productCode + ")";
                    vProduct.priceEach = res.priceEach;
                    vProduct.quantityInStock = res.quantityInStock;
                }, error: function (err) {
                    vProduct = null;
                    console.log(err.responseText);
                }
            });
        } else {
            toastr.error("You are not logged in!");
        }
        return vProduct;
    }

    //Hàm load dữ liệu vào List Order Detail
    function loadDataToOrderDetailsList() {
        "use strict";
        $("#tbody-orderDetails").html("");
        var vTextOrderDetails = "";
        var vTotalQuantityOrder = 0;
        var vOrderAmount = 0;
        gOrderDetails.forEach(element => {
            vTotalQuantityOrder += Number(element.quantityOrder);
            vOrderAmount += element.quantityOrder * element.priceEach;
            vTextOrderDetails +=
                `<tr class="text-primary">
                    <td>${element.productName}</td>
                    <td>${element.quantityOrder}</td>
                    <td>${element.priceEach.toFixed(2).toString().replace(/\B(?=(\d{3})(?!\d))/g, ",")}</td>
                    <td>${(element.quantityOrder * element.priceEach).toFixed(2).toString().replace(/\B(?=(\d{3})(?!\d))/g, ",")}</td>
                    <td class="text-center"><a href="javascript:void(0)" title="Remove"><i data-product-id="${element.productId}"
                                class="fa-solid fa-circle-xmark text-danger remove-order-detail"></i></a></td>
                </tr>`;
        });
        $("#tbody-orderDetails").append(vTextOrderDetails);
        $("#th-quantityOrder").html(vTotalQuantityOrder);
        $("#th-orderAmount").html("$" + vOrderAmount.toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","));
    }
});