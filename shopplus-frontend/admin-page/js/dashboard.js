"use strict";
$(document).ready(function () {
    /*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
    moment.updateLocale("en", { week: { dow: 1 } });
    var gStartDateTopSelling = moment().subtract(6, 'days').format("YYYY-MM-DD");
    var gEndDateTopSelling = moment().format("YYYY-MM-DD");
    const gNameCol = ["id", "productCode", "fullName", "buyPrice", "discountPercentage", "totalOrders", "totalSales", "quantityInStock"];
    const gID_COL = 0;
    const gPRODUCT_CODE_COL = 1;
    const gFULL_NAME_COL = 2;
    const gBUY_PRICE_COL = 3;
    const gDISCOUNT_PERCENTAGE_COL = 4;
    const gTOTAL_ORDERS_COL = 5;
    const gTOTAL_SALES_COL = 6;
    const gQUANTITY_IN_STOCK_COL = 7;
    var gDataTable = $("#top-selling-table").DataTable({
        columns: [
            { data: gNameCol[gID_COL] },
            { data: gNameCol[gPRODUCT_CODE_COL] },
            { data: gNameCol[gFULL_NAME_COL] },
            { data: gNameCol[gBUY_PRICE_COL] },
            { data: gNameCol[gDISCOUNT_PERCENTAGE_COL] },
            { data: gNameCol[gTOTAL_ORDERS_COL] },
            { data: gNameCol[gTOTAL_SALES_COL] },
            { data: gNameCol[gQUANTITY_IN_STOCK_COL] }
        ],
        columnDefs: [
            {
                targets: gFULL_NAME_COL,
                className: "text-nowrap"
            },
            {
                targets: gBUY_PRICE_COL,
                render: function (data, type) {
                    if (data != null) {
                        return data.toFixed(2).replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                    }
                    return "";
                }
            },
            {
                targets: gDISCOUNT_PERCENTAGE_COL,
                render: function (data, type) {
                    if (data != null) {
                        return data + "%";
                    }
                    return "";
                }
            },
            {
                targets: gTOTAL_ORDERS_COL,
                render: function (data, type, row, meta) {
                    var vTotalOrders = getTotalOrdersProduct(gStartDateTopSelling, gEndDateTopSelling, row["id"]);
                    return vTotalOrders;
                }
            }
            ,
            {
                targets: gTOTAL_SALES_COL,
                render: function (data, type, row, meta) {
                    var vTotalSales = getTotalSalesProduct(gStartDateTopSelling, gEndDateTopSelling, row["id"]);
                    return vTotalSales.toFixed(0).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                }
            }
        ],
        ordering: false,
        searching: false,
        paging: false,
        info: false,
        scrollX: true,
    });

    /*** REGION 2 - Vùng gán / thực thi sự kiện cho các elements */
    onPageLoading();

    //Gán sự kiện cho select Annual Sales
    $("#sel-annual-sales").on("change", function () {   
        loadDataToChartAnnualSales();
    });

    //Gán sự kiện cho select Weekly Sales
    $("#sel-weekly-sales").on("change", function () {
        loadOrderDataToWeeklySales();
    });

    //Gán sự kiện cho select Last days
    $("#sel-last-days").on("change", function () {
        var vLastDays = $("#sel-last-days").val();
        gStartDateTopSelling = moment().subtract(vLastDays - 1, 'days').format("YYYY-MM-DD");
        gEndDateTopSelling = moment().format("YYYY-MM-DD");
        loadProductDataToTable();
    });

    //Gán sự kiện cho select Top Products
    $("#sel-top-products").on("change", function () {
        loadProductDataToTable();
    });

    //Điều chỉnh lại các cột table khi thay đổi kích thước window
    $(window).resize(function () {
        $($.fn.dataTable.tables(true)).DataTable().columns.adjust();
    });


    /*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
    function onPageLoading() {
        "use strict";
        var vDailySales = getTotalSales(moment().format("YYYY-MM-DD"), moment().format("YYYY-MM-DD"));
        $("#h-daily-sales").html(vDailySales ? "$" + vDailySales.toFixed(0).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") : "$0");

        var vNewOrders = getTotalOrders(moment().format("YYYY-MM-DD"), moment().format("YYYY-MM-DD"));
        $("#h-new-orders").html(vNewOrders);

        var vNewUsers = getTotalUsers(moment().format("YYYY-MM-DD"), moment().format("YYYY-MM-DD"));
        $("#h-new-users").html(vNewUsers);

        loadDataToChartAnnualSales();

        loadOrderDataToWeeklySales();

        loadProductDataToTable();
    }

    /*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
    // Hàm load dữ liệu Product vào bảng
    function loadProductDataToTable() {
        "use strict";
        var token = getCookie("token");
        if (token) {
            var vTopProductSelect = $("#sel-top-products").val();
            var vTopProductUrl = ""
            if (vTopProductSelect == "1") {
                vTopProductUrl = "products/top-selling-by-total-orders";
            } else {
                vTopProductUrl = "products/top-selling-by-total-sales";
            }
            $.ajax({
                url: gBASE_URL + vTopProductUrl + "?startDateString=" + gStartDateTopSelling + "&endDateString=" + gEndDateTopSelling,
                method: "GET",
                headers: {
                    Authorization: "Bearer " + token
                },
                success: function (res) {
                    gDataTable.clear();
                    gDataTable.rows.add(res.content);
                    gDataTable.draw();
                    $($.fn.dataTable.tables(true)).DataTable().columns.adjust();
                },
                error: function (err) {
                    console.log(err.responseText);
                }
            });
        } else {
            toastr.error("You are not logged in!");
        }
    }

    // Hàm load dữ liệu ChartAnnualSales
    function loadDataToChartAnnualSales() {
        "use strict";
        $("#chart-1").html("<canvas id='chart-annual-sales' style='width: 100%; height: 400px'></canvas>")
        var ctx1 = document.getElementById("chart-annual-sales").getContext("2d");
        var vAnnualSales = 0;
        var vMonthlySales = 0;
        var vSelectAnnualSalesValue = $("#sel-annual-sales").val();
        if (vSelectAnnualSalesValue == "1") {
            vMonthlySales = getDataMonthlySalesThisYear();
            vAnnualSales = getTotalSales(moment().startOf("year").format("YYYY-MM-DD"), moment().endOf("year").format("YYYY-MM-DD"));
        } else {
            vMonthlySales = getDataMonthlySalesLastYear();
            vAnnualSales = getTotalSales(moment().subtract(1, "year").startOf("year").format("YYYY-MM-DD"), moment().subtract(1, "year").endOf("year").format("YYYY-MM-DD"));
        }
        $("#total-annual-sales").html(vAnnualSales ? "$" + vAnnualSales.toFixed(0).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") : "$0");
        
        new Chart(ctx1, {
            // The type of chart we want to create
            type: "line", // also try bar or other graph types

            // The data for our dataset
            data: {
                labels: [
                    "Jan",
                    "Feb",
                    "Mar",
                    "Apr",
                    "May",
                    "Jun",
                    "Jul",
                    "Aug",
                    "Sep",
                    "Oct",
                    "Nov",
                    "Dec",
                ],
                // Information about the dataset
                datasets: [
                    {
                        label: "",
                        backgroundColor: "transparent",
                        borderColor: "#4A6CF7",
                        data: vMonthlySales,
                        pointBackgroundColor: "transparent",
                        pointHoverBackgroundColor: "#4A6CF7",
                        pointBorderColor: "transparent",
                        pointHoverBorderColor: "#fff",
                        pointHoverBorderWidth: 5,
                        pointBorderWidth: 5,
                        pointRadius: 8,
                        pointHoverRadius: 8,
                    },
                ],
            },

            // Configuration options
            defaultFontFamily: "Inter",
            options: {
                tooltips: {
                    callbacks: {
                        labelColor: function (tooltipItem, chart) {
                            return {
                                backgroundColor: "#ffffff",
                            };
                        },
                        label: function (tooltipItem, data) {
                            return "$" + tooltipItem.yLabel.toFixed(0).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                        }
                    },
                    intersect: false,
                    backgroundColor: "#f9f9f9",
                    titleFontFamily: "Inter",
                    titleFontColor: "#8F92A1",
                    titleFontColor: "#8F92A1",
                    titleFontSize: 12,
                    bodyFontFamily: "Inter",
                    bodyFontColor: "#171717",
                    bodyFontStyle: "bold",
                    bodyFontSize: 16,
                    multiKeyBackground: "transparent",
                    displayColors: false,
                    xPadding: 30,
                    yPadding: 10,
                    bodyAlign: "center",
                    titleAlign: "center",
                },

                title: {
                    display: false,
                },
                legend: {
                    display: false,
                },

                scales: {
                    yAxes: [
                        {
                            gridLines: {
                                display: false,
                                drawTicks: false,
                                drawBorder: false,
                            },
                            ticks: {
                                padding: 35,
                                max: 500000,
                                min: 0,
                                userCallback: function (value, index, values) {
                                    return value.toFixed(0).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                                }
                            },
                        },
                    ],
                    xAxes: [
                        {
                            gridLines: {
                                drawBorder: false,
                                color: "rgba(143, 146, 161, .1)",
                                zeroLineColor: "rgba(143, 146, 161, .1)",
                            },
                            ticks: {
                                padding: 20,
                            },
                        },
                    ],
                },
            },
        });
    }

    // Hàm load dữ liệu ChartWeeklySales
    function loadOrderDataToWeeklySales() {
        "use strict";
        $("#chart-2").html("<canvas id='chart-weekly-sales' style='width: 100%; height: 400px'></canvas>")
        const ctx2 = document.getElementById("chart-weekly-sales").getContext("2d");
        var vDailySales = 0;
        var vWeeklySales = 0;
        var vSelectWeeklySalesValue = $("#sel-weekly-sales").val();
        if (vSelectWeeklySalesValue == "1") {
            vDailySales = getDataDailySalesThisWeek();
            vWeeklySales = getTotalSales(moment().startOf("week").format("YYYY-MM-DD"), moment().endOf("week").format("YYYY-MM-DD"));
        } else {
            vDailySales = getDataDailySalesLastWeek();
            vWeeklySales = getTotalSales(moment().subtract(1, "week").startOf("week").format("YYYY-MM-DD"), moment().subtract(1, "week").endOf("week").format("YYYY-MM-DD"));
        }
        $("#total-weekly-sales").html(vWeeklySales ? "$" + vWeeklySales.toFixed(0).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") : "$0");
        
        new Chart(ctx2, {
            // The type of chart we want to create
            type: "bar", // also try bar or other graph types
            // The data for our dataset
            data: {
                labels: [
                    "Mon",
                    "Tue",
                    "Wed",
                    "Thu",
                    "Fri",
                    "Sat",
                    "Sun"
                ],
                // Information about the dataset
                datasets: [
                    {
                        label: "",
                        backgroundColor: "#4A6CF7",
                        barThickness: 6,
                        maxBarThickness: 8,
                        data: vDailySales,
                    },
                ],
            },
            // Configuration options
            options: {
                borderColor: "#F3F6F8",
                borderWidth: 15,
                backgroundColor: "#F3F6F8",
                tooltips: {
                    callbacks: {
                        labelColor: function (tooltipItem, chart) {
                            return {
                                backgroundColor: "rgba(104, 110, 255, .0)",
                            };
                        },
                        label: function (tooltipItem, data) {
                            return "$" + tooltipItem.yLabel.toFixed(0).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                        }
                    },
                    backgroundColor: "#F3F6F8",
                    titleFontColor: "#8F92A1",
                    titleFontSize: 12,
                    bodyFontColor: "#171717",
                    bodyFontStyle: "bold",
                    bodyFontSize: 16,
                    multiKeyBackground: "transparent",
                    displayColors: false,
                    xPadding: 30,
                    yPadding: 10,
                    bodyAlign: "center",
                    titleAlign: "center",
                },

                title: {
                    display: false,
                },
                legend: {
                    display: false,
                },

                scales: {
                    yAxes: [
                        {
                            gridLines: {
                                display: false,
                                drawTicks: false,
                                drawBorder: false,
                            },
                            ticks: {
                                padding: 35,
                                max: 20000,
                                min: 0,
                                userCallback: function (value, index, values) {
                                    return value.toFixed(0).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                                }
                            },
                        },
                    ],
                    xAxes: [
                        {
                            gridLines: {
                                display: false,
                                drawBorder: false,
                                color: "rgba(143, 146, 161, .1)",
                                zeroLineColor: "rgba(143, 146, 161, .1)",
                            },
                            ticks: {
                                padding: 20,
                            },
                        },
                    ],
                },
            },
        });
    }

    //Hàm tính tổng doanh thu
    function getTotalSales(paramStartDate, paramEndDate) {
        "use strict";
        var vSales = 0;
        var token = getCookie("token");
        if (token) {
            $.ajax({
                url: gBASE_URL + "orders/total-sales?startDateString=" + paramStartDate + "&endDateString=" + paramEndDate,
                method: "GET",
                async: false,
                headers: {
                    Authorization: "Bearer " + token
                },
                success: function (res) {
                    vSales = res;
                },
                error: function (err) {
                    console.log(err.responseText);
                }
            });
        } else {
            toastr.error("You are not logged in!");
        }
        return vSales;
    }

    //Hàm lấy số đơn hàng mới trong ngày
    function getTotalOrders(paramStartDate, paramEndDate) {
        "use strict";
        var vTotalOrders = 0;
        var token = getCookie("token");
        if (token) {
            $.ajax({
                url: gBASE_URL + "orders/total-orders?startDateString=" + paramStartDate + "&endDateString=" + paramEndDate,
                method: "GET",
                async: false,
                headers: {
                    Authorization: "Bearer " + token
                },
                success: function (res) {
                    vTotalOrders = res;
                },
                error: function (err) {
                    console.log(err.responseText);
                }
            });
        } else {
            toastr.error("You are not logged in!");
        }
        return vTotalOrders;
    }

    //Hàm lấy số user mới trong ngày
    function getTotalUsers(paramStartDate, paramEndDate) {
        "use strict";
        var vTotalUsers = 0;
        var token = getCookie("token");
        if (token) {
            $.ajax({
                url: gBASE_URL + "users/total-users?startDateString=" + paramStartDate + "&endDateString=" + paramEndDate,
                method: "GET",
                async: false,
                headers: {
                    Authorization: "Bearer " + token
                },
                success: function (res) {
                    vTotalUsers = res;
                },
                error: function (err) {
                    console.log(err.responseText);
                }
            });
        } else {
            toastr.error("You are not logged in!");
        }
        return vTotalUsers;
    }

    //Hàm lấy dữ liệu doanh thu hằng tháng trong năm nay
    function getDataMonthlySalesThisYear() {
        "use strict";
        var vMonthlySales = [];
        var vYear = moment().year();
        for (var bI = 0; bI <= moment().month(); bI++) {
            var bDay = moment([vYear, bI, 1]);
            var bSales = getTotalSales(bDay.format("YYYY-MM-DD"), bDay.endOf("month").format("YYYY-MM-DD"));
            vMonthlySales.push(bSales);
        }
        return vMonthlySales;
    }

    //Hàm lấy dữ liệu doanh thu hằng tháng trong năm ngoái
    function getDataMonthlySalesLastYear() {
        "use strict";
        var vMonthlySales = [];
        var vYear = moment().subtract(1, "year").year();
        for (var bI = 0; bI < 12; bI++) {
            var bDay = moment([vYear, bI, 1]);
            var bSales = getTotalSales(bDay.format("YYYY-MM-DD"), bDay.endOf("month").format("YYYY-MM-DD"));
            vMonthlySales.push(bSales);
        }
        return vMonthlySales;
    }

    //Hàm lấy dữ liệu doanh thu hằng ngày trong tuần này
    function getDataDailySalesThisWeek() {
        "use strict";
        var vDailySales = [];
        var vWeekDay = moment().weekday();
        for (var bI = 0; bI <= vWeekDay; bI++) {
            var bDay = moment().startOf("week").add(bI, "days").format("YYYY-MM-DD");
            var bSales = getTotalSales(bDay, bDay);
            vDailySales.push(bSales);
        }
        return vDailySales;
    }

    //Hàm lấy dữ liệu doanh thu hằng ngày trong tuần trước
    function getDataDailySalesLastWeek() {
        "use strict";
        var vDailySales = [];
        var vDayLastWeek = moment().subtract(1, "week");
        for (var bI = 0; bI < 7; bI++) {
            var bDay = vDayLastWeek.startOf("week").add(bI, "days").format("YYYY-MM-DD");
            var bSales = getTotalSales(bDay, bDay);
            vDailySales.push(bSales);
        }
        return vDailySales;
    }

    //Hàm lấy tổng số đơn hàng theo sản phẩm
    function getTotalOrdersProduct(paramStartDate, paramEndDate, paramProductId) {
        "use strict";
        var vTotalOrders = 0;
        var token = getCookie("token");
        if (token) {
            $.ajax({
                url: gBASE_URL + "products/total-orders-product?startDateString=" + paramStartDate + "&endDateString=" + paramEndDate + "&productId=" + paramProductId,
                method: "GET",
                async: false,
                headers: {
                    Authorization: "Bearer " + token
                },
                success: function (res) {
                    vTotalOrders = res;
                },
                error: function (err) {
                    console.log(err.responseText);
                }
            });
        } else {
            toastr.error("You are not logged in!");
        }
        return vTotalOrders;
    }

    //Hàm lấy tổng số doanh thu theo sản phẩm
    function getTotalSalesProduct(paramStartDate, paramEndDate, paramProductId) {
        "use strict";
        var vTotalSales = 0;
        var token = getCookie("token");
        if (token) {
            $.ajax({
                url: gBASE_URL + "products/total-sales-product?startDateString=" + paramStartDate + "&endDateString=" + paramEndDate + "&productId=" + paramProductId,
                method: "GET",
                async: false,
                headers: {
                    Authorization: "Bearer " + token
                },
                success: function (res) {
                    vTotalSales = res;
                },
                error: function (err) {
                    console.log(err.responseText);
                }
            });
        } else {
            toastr.error("You are not logged in!");
        }
        return vTotalSales;
    }
});