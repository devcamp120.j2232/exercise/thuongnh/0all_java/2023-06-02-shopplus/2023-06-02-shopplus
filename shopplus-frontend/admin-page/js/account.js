$(document).ready(function () {
    "use strict";
    /*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
    var gStrRole = "";

    /*** REGION 2 - Vùng gán / thực thi sự kiện cho các elements */
    onPageLoading();

    //Gán sự kiện cho nút đăng xuất
    $("#li-logout").on("click", onLogoutClick);

    /*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
    function onPageLoading() {
        "use strict";
        var token = getCookie("token");
        if (token) {
            var headers = {
                Authorization: "Bearer " + token
            };
            $.ajax({
                url: gBASE_URL + "auth/user-info",
                method: "GET",
                headers: headers,
                success: function (res) {
                    $("#h-fullname").html(res.firstName + " " + res.lastName)
                    checkPermission(res.strRoles);
                },
                error: function (err) {
                    console.log(err);
                }
            });
        } else {
            window.location.href = "login.html";
        }
    }

    //Hàm xử lý khi ấn Đăng xuất
    function onLogoutClick() {
        "use strict";
        //Xóa cookie
        setCookie("token", "", 10, false);
        window.location.href = "login.html";
    }

    /*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
    //Hàm kiểm tra permission
    function checkPermission(paramStrRoles) {
        "use strict";
        if(paramStrRoles.includes("ROLE_ADMIN")) {
            gStrRole = "ROLE_ADMIN";
            $("#menu-dashboard").prop("hidden", false);
            $("#menu-orders").prop("hidden", false);
            $("#menu-payments").prop("hidden", false);
            $("#menu-products").prop("hidden", false);
            $("#menu-processors").prop("hidden", false);
            $("#menu-graphics").prop("hidden", false);
            $("#menu-employees").prop("hidden", false);
            $("#menu-offices").prop("hidden", false);
            $("#menu-users").prop("hidden", false);
        } else if(paramStrRoles.includes("ROLE_SELLER")) {
            gStrRole = "ROLE_SELLER";
            $("#menu-dashboard").prop("hidden", true);
            $("#menu-orders").prop("hidden", false);
            $("#menu-payments").prop("hidden", false);
            $("#menu-products").prop("hidden", true);
            $("#menu-processors").prop("hidden", true);
            $("#menu-graphics").prop("hidden", true);
            $("#menu-employees").prop("hidden", true);
            $("#menu-offices").prop("hidden", true);
            $("#menu-users").prop("hidden", false);
        } else {
            setCookie("token", "", 10, false);
            window.location.href = "login.html";
        }
    }
});